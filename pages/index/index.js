//index.js
//获取应用实例
const app = getApp()
const utils = require('../../utils/util.js')
const common = require('../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    imgUrls: {},
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    circular: true,
    interval: 2000,
    duration: 500,
    previousMargin: 0,
    nextMargin: 0,
    userInfo: {},
    array: {},
    commendlist: [],
    hasData: false,
    hasUserInfo: false,
    canIUse: tt.canIUse('button.open-type.getUserInfo'),
    data: {},
    timestamp: '',
    categoryList: {},
    brandsList: {},
    ad01: '',
    ad02: '',
    ad03: '',
    ad04: '',
    ad05: '',
    page: 1,
    background_color: '#FFFFFF',
    show_loading: true,
    current_page: 1,
    total_page: 1,
    show_flag: false,
    category_height: 'height:250rpx;',
    show_authorize: false,
    shop_name: '',
    share_picture: '',
    top_button_show: false,
    windwos_height: 680,
    ret_share_user_id: '',
    is_sharer: false,
  },
  onLoad: function(options) {
    this.setData({
      _t: utils._t(), //翻译
      language: tt.getStorageSync('Language')
    });

    var that = this;
    let user_id = ''
    let share_user_id = ''
    var openid = app.globalData.openid

    tt.showLoading({
      title: _('加载中'),
      mask: 'true'
    })

    console.log('options')
    console.log(options)
    console.log('options')

    //如果来源于二维码扫码url参数解析
    if (options.q !== undefined) {
      console.log('来源于二维码扫码url参数解析')
      var scan_url = decodeURIComponent(options.q);
      var result = utils.parseURL(scan_url);
      console.log('result:' + result)
      user_id = result.s
      share_user_id = result.r
      if (result.ret) {
        share_user_id = app.globalData.share_user_id = result.ret
        that.setData({
          ret_share_user_id: share_user_id
        })
      }
    } else { //否则来源于页面迁移和分享点击
      console.log('来源于分享点击')
      user_id = options.s
      share_user_id = options.r
    }

    console.log('Ret share_id: ' + that.data.ret_share_user_id + '--' + share_user_id)

    //判断远程请求中是否有user_id，如果有，使用url请求中的user_id
    if (user_id) {
      app.globalData.user_id = user_id
    } else {
      user_id = app.globalData.user_id
    }

    //判断远程请求中是否有share_user_id，如果有，使用url请求中的share_user_id
    if (share_user_id) {
      app.globalData.share_user_id = share_user_id
    } else {
      share_user_id = app.globalData.share_user_id
    }

    //检查是否获取openid，如果未获取，去获取openid
    if (app.globalData.openid == '') {
      app.thisOpenidCallback = res => {
        this.initPageData(user_id)
      }
    } else {
      this.initPageData(user_id)
    }
  },
  initPageData: function(user_id) {
    let that = this
    let openid = app.globalData.openid
    var timestamp = (new Date()).valueOf();

    that.setData({
      windwos_height: tt.getSystemInfoSync().windowHeight,
      is_sharer: app.globalData.is_sharer
    })

    console.log(openid)
    console.log(that.data.ret_share_user_id)

    if (openid && that.data.ret_share_user_id) {
      that.toBindShareUserIdAndOpenId();
    }
    var form_data = {
      user_id: user_id,
      share_user_id: app.globalData.share_user_id,
      openid: openid
    }

    console.log('ajaxGetDefaultShopId:')
    console.log(form_data)
    console.log('ajaxGetDefaultShopId:')
    
    //获取系统通知内容
    utils.requestQueryByPost({
      url: '/mobile/api/ajaxSystemNotice',
      form_data: form_data,
      success: function (res) {
        if (res.data.success) {
          that.setData({
            notice: res.data.data
          })
        }
      }
    })

    // 获取默认店铺id
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetDefaultShopId',
      form_data: {
        user_id: user_id,
        share_user_id: app.globalData.share_user_id,
        openid: openid
      },
      success: function(response) {
        if (response.data.success) {
          app.globalData.user_id = response.data.data.id
          app.globalData.share_user_id = response.data.data.share_user_id
          console.log(response.data.data)
          that.setData({
            shop_name: response.data.data.shop_name,
            share_desc: response.data.data.share_desc,
            service_tel: response.data.data.service_tel,
            share_picture: response.data.data.share_picture
          })
          app.globalData.shop_info = response.data.data
          console.log(response)
        }
      },
      complete: function() {
        //判断远程请求中是否有user_id，如果有，使用url请求中的user_id
        if (user_id) {
          app.globalData.user_id = user_id
        } else {
          user_id = app.globalData.user_id
        }
        utils.requestQueryByPost({
          url: '/mobile/wechat/ajaxSlideImage',
          form_data: {
            user_id: user_id,
            openid: openid
          },
          success: function(res) {
            if (res.data.success) {
              that.setData({
                imgUrls: res.data.data,
                timestamp: timestamp
              })
            }
          },
          fail: function() {
            that.setData({
              imgUrls: [
                '/resoures/images/slide_01.jpg',
                '/resoures/images/slide_02.jpg',
                '/resoures/images/slide_03.jpg'
              ]
            })
          }
        })
        utils.requestQueryByPost({
          url: '/mobile/wechat/ajaxAdImage',
          form_data: {
            user_id: user_id,
            openid: openid
          },
          success: function(res) {
            if (res.data.success) {
              that.setData({
                ad01: res.data.data.ad01,
                ad02: res.data.data.ad02,
                ad03: res.data.data.ad03,
                ad04: res.data.data.ad04,
                ad05: res.data.data.ad05,
                timestamp: timestamp
              })
            }
          }
        })
        utils.requestQueryByPost({
          url: '/mobile/wechat/ajaxSmallAppBackgroundColor',
          form_data: {
            user_id: user_id,
            openid: openid
          },
          success: function(res) {
            if (res.data.success) {
              that.setData({
                background_color: res.data.data.background_color
              })
            }
          }
        })
        //获取首页商品列表
        utils.requestQueryByPost({
          url: '/mobile/wechat/ajaxGetGoodsForToppage_2_0',
          form_data: {
            user_id: user_id,
            is_sharer: that.data.is_sharer,
            share_user_id: app.globalData.share_user_id,
            openid: openid
          },
          success: function(response) {
            that.setData({
              data: response.data.data,
              show_flag: true
            })
          },
          complete: function() {
            tt.hideLoading()
          }
        })
        //获取首页商品分类按钮
        utils.requestQueryByPost({
          url: '/mobile/wechat/ajaxGetTopCategorys',
          form_data: {
            user_id: user_id,
            openid: openid
          },
          success: function(response) {
            if (response.data.success) {
              that.setData({
                categoryList: response.data.data.data_list
              })
              if (response.data.data.data_list.length > 4) {
                that.setData({
                  category_height: 'height:500rpx;'
                })
              }
            }
          }
        })
        //获取首页品牌分类按钮
        utils.requestQueryByPost({
          url: '/mobile/wechat/ajaxGetTopBrands',
          form_data: {
            user_id: user_id,
            openid: openid
          },
          success: function(response) {
            if (response.data.success) {
              that.setData({
                brandsList: response.data.data
              })
            }
          }
        })
        //获取推荐商品列表
        that.getCommendGoodsList();
      }
    })
  },
  //事件处理函数
  bindGoodsViewTap: function(e) {
    var hash = e.currentTarget.dataset.hash
    console.log(app.globalData.share_user_id)
    tt.navigateTo({
      url: '../detail/detail?h=' + hash
    })
  },
  bindAdViewTap: function() {
    tt.navigateTo({
      url: '../ad/ad'
    })
  },
  bindSearchView: function(e) {
    var search_key = e.detail.value
    if (!search_key) {
      search_key = e.currentTarget.dataset.searchkey
    }
    if (search_key) {
      tt.navigateTo({
        url: '../list/list?search_key=' + search_key
      })
    } else {
      tt.navigateTo({
        url: '../list/list?search_key=神仙水'
      })
    }
  },
  bindDominoView: function(e) {
    var user_id = app.globalData.user_id
    tt.navigateTo({
      url: '../domino/domino?s=' + user_id
    })
  },
  bindCartViewTap: function(e) {
    tt.switchTab({
      url: '../cart/cart'
    });
  },
  //处理加入购物车
  bindAddCartViewTap: function(e) {
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var openid = app.globalData.openid
    var goods_hash = e.currentTarget.dataset.hash
    var goods_quantity = 1
    common.addGoodsToCart(user_id, share_user_id, openid, goods_hash, goods_quantity)
  },
  bindOrdersViewTap: function() {
    tt.navigateTo({
      url: '../orders/orders'
    });
  },
  bindWishViewTap: function() {
    tt.navigateTo({
      url: '../wish/wish'
    });
  },
  bindToTopTap: function() {
    //窗口滚动到最顶端
    tt.pageScrollTo({
      scrollTop: 0,
      duration: 0
    })
  },
  bindListViewTap: function(e) {
    var d = e.currentTarget.dataset
    var goodstype = d.goodstype ? d.goodstype : ''
    var category_large = d.categoryid ? d.categoryid : ''
    var brandid = d.brandid ? d.brandid : ''
    var url = '../list/list?goodstype=' + goodstype + '&brandid=' + brandid + '&category_large=' + category_large
    tt.navigateTo({
      url: url
    });
  },
  bindBrandsViewTap: function(e) {
    tt.navigateTo({
      url: '../brands/brands'
    })
  },
  bindCategoryViewTap: function(e) {
    tt.switchTab({
      url: '../category/category'
    })
  },
  bindPrivacyViewTap: function() {
    tt.navigateTo({
      url: '../posts/posts?query=privacy'
    })
  },
  bindServicesViewTap: function() {
    tt.navigateTo({
      url: '../posts/posts?query=services'
    })
  },
  onShareAppMessage: function(res) {
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    if (res.from === 'button') {
      // 来自页面内转发按钮
    }
    if (this.data.share_picture != '') {
      return {
        title: this.data.share_desc,
        path: '/pages/index/index?s=' + user_id + '&r=' + share_user_id,
        imageUrl: this.data.share_picture
      }
    } else {
      return {
        title: this.data.share_desc,
        path: '/pages/index/index?s=' + user_id + '&r=' + share_user_id
      }
    }
  },
  onReachBottom: function() {
    if (this.data.current_page < this.data.total_page) {
      var page = this.data.current_page + 1;
      this.setData({
        show_loading: true,
        page: page
      })
      this.getCommendGoodsList()
    }
  },
  onPageScroll: function(e) {
    if (e.scrollTop > this.data.windwos_height * 3) {
      this.setData({
        top_button_show: true
      })
    } else {
      this.setData({
        top_button_show: false
      })
    }
  },
  getCommendGoodsList: function() {
    
    var that = this
    var openid = app.globalData.openid
    var goods_data = this.data.commendlist
    var share_user_id = app.globalData.share_user_id
    that.setData({
      is_sharer: app.globalData.is_sharer
    })

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGoodsList',
      form_data: {
        user_id: app.globalData.user_id,
        openid: openid,
        share_user_id: share_user_id,
        is_sharer: that.data.is_sharer,
        page: that.data.page,
        is_commend: 1
      },
      success: function(response) {
        if (response.data.success) {
          for (var append_index in response.data.data.goods_list) {
            goods_data.push(response.data.data.goods_list[append_index]);
          }
          that.setData({
            commendlist: goods_data,
            show_loading: false,
            current_page: response.data.data.current_page,
            total_page: response.data.data.total_page,
            total_items: response.data.data.total
          })
        }
      }
    })
  },
  onReady: function() {
    this.setData({
      //show_flag: true
    })
  },
  onShow: function() {
    //由于需要切换店铺判断是否需要刷新首页数据
    console.log('进入首页onshow方法')
    var user_id = app.globalData.user_id
    if (app.globalData.index_is_reload) {
      console.log('判断index_is_reload是true，重新刷新首页')
      this.initPageData(user_id)
      app.globalData.index_is_reload = false
    }
  },
  toBindShareUserIdAndOpenId: function() {
    var that = this
    var openid = app.globalData.openid
    var share_user_id = that.data.ret_share_user_id

    console.log({
      user_id: app.globalData.user_id,
      openid: openid,
      share_user_id: share_user_id
    })

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/retail/ajaxBindShareUserIdAndOpenid',
      form_data: {
        user_id: app.globalData.user_id,
        openid: openid,
        share_user_id: share_user_id
      },
      success: function(response) {
        console.log(response)
        if (response.data.success) {
          app.globalData.user_id = response.data.data.user_id
          tt.showToast({ //显示返回结果
            title: response.data.message,
            icon: 'none',
            duration: 4000
          })
        }
      }
    })
  }
})