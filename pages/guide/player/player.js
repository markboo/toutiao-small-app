const app = getApp()
const utils = require('../../../utils/util.js')
const common = require('../../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    userInfo: {}
  },
  onLoad: function ( options ) {

    this.setData({
        moive_url: options.u,
        moive_name: options.n
    })

    this.setData({
      _t: utils._t(), //翻译
      language: wx.getStorageSync('Language')
    });

    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    }
  },
  //返回
  bindBackViewTap: function () {
    wx.navigateBack({})
  }
})