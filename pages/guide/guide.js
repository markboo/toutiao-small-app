const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
  data: {
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    moive_list: [],
    userInfo: {}
  },
  onLoad: function() {

    this.setData({
      _t: utils._t(), //翻译
      language: wx.getStorageSync('Language')
    });
    
    wx.showLoading({
      title: _('加载中'),
      success: function() {}
    })
    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    }

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGuideMovie',
      form_data: {},
      success: function(response) {
        console.log(response)
        that.setData({
          moive_list: response.data.data
        })        
        wx.hideLoading()
      }
    })
  },   
  //返回
   bindBackViewTap: function () {
    wx.navigateBack({})
  },
  bindPlayerViewTap: function(e) {
    console.log(this.data.moive_list)
    var movie = this.data.moive_list[e.currentTarget.dataset.movieid];
    wx.navigateTo({
      url: '../guide/player/player?n=' + movie.contents + '&u=' + movie.movie_url
    })
  }
})