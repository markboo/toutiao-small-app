const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
  data: {
    array: {},
    region: {},
    address_data: {},
    hasAddressFront: false,
    hasAddressBack: false,
    hash: '',
    tempFilePaths: {
      'front': '',
      'back': ''
    },
    timestamp: ''
  },
  onLoad: function(option) {

    this.setData({
      _t: utils._t(), //翻译
    });

    var that = this
    var hash = option.addressid
    //如果hash不为空，这是编辑状态
    if (hash != 'undefined') {
      //加载窗
      wx.showLoading({
        title: _('加载中')
      })

      var timestamp = (new Date()).valueOf();

      wx.getLocation({
        type: 'gcj02',
        success: function(res) {
          console.log(res)
        }
      })
      //获取地址详情
      utils.requestQueryByPost({
        url: '/mobile/wechat/getAddressDetail',
        form_data: {
          hash: hash
        },
        success: function(res) {
          var address_data = res.data.data
          that.setData({
            hash: hash,
            address_data: address_data,
            //设置地址
            region: [address_data.recipient_province_text, address_data.recipient_city_text, address_data.recipient_district_text],
            timestamp: timestamp
          })
          //设置身份证明照片
          if (address_data.card_photo_front != "") {
            that.setData({
              hasAddressFront: true
            })
          }
          //设置身份背面照片
          if (address_data.card_photo_back != "") {
            that.setData({
              hasAddressBack: true
            })
          }
          //更新图片存储
          that.setData({
            tempFilePaths: {
              'front': address_data.card_photo_front,
              'back': address_data.card_photo_back
            }
          })
        },
        complete: function() {
          wx.hideLoading() //关闭加载框
        }
      });
    } else {
      //如果不是编辑状态而是新建状态，则初始化地址选择框
      that.setData({
        region: ['', '...', '...']
      })
    }
  },
  //返回
  bindBackViewTap: function() {
    wx.navigateBack({})
  },
  //地址选择改变
  bindRegionChange: function(e) {
    this.setData({
      region: e.detail.value
    })
  },
  //选择图片
  bindChooseImage: function(e) {
    var that = this
    var picture = e.currentTarget.dataset.picture
    wx.chooseImage({
      count: 1,
      success: function(res) {
        that.data.tempFilePaths[picture] = res.tempFilePaths[0]
        that.setData({
          tempFilePaths: that.data.tempFilePaths,
          hasAddressFront: false,
          hasAddressBack: false
        })
      }
    })
  },
  /**
   * 处理提交保存地址
   */
  bindSaveAddressTap: function(e) {
    var that = this
    //新建地址的时候hash是空的
    var hash = this.data.hash
    e.detail.value.recipient_province_text = this.data.region[0]
    e.detail.value.recipient_city_text = this.data.region[1]
    e.detail.value.recipient_district_text = this.data.region[2]
    e.detail.value.openid = app.globalData.openid
    e.detail.value.user_id = app.globalData.user_id
    //新建地址的时候hash是空的
    e.detail.value.hash = this.data.hash

    var checked_result = that.checkUpdateData(e.detail) //校验页面提交数据

    if (checked_result.is_checked) { //校验结果为：成功
      if (e.detail.value.recipient_id == '') {
        utils.requestQueryByPost({
          url: '/mobile/wechat/uploadFiles',
          form_data: e.detail.value,
          success: function(res) {
            console.log('校验结果为：成功,执行uploadFiles结果：' + res.data.success)
            wx.hideLoading() //关闭提交窗
            if (res.data.success) {
              wx.showToast({ //显示返回结果
                title: res.data.message,
                icon: 'none',
                duration: 1500,
                success: function() {
                  var timer = setTimeout(function() {
                    clearTimeout(timer)
                    wx.navigateBack({})
                  }, 1500)

                }
              })
            }
            wx.showToast({ //显示返回结果
              title: res.data.message,
              icon: 'none',
              duration: 3000
            })
          },
          fail: function() {
            wx.hideLoading() //关闭提交窗
          }
        })
      } else {
        try {
          that.uploadfile({
            file: that.data.tempFilePaths['front'], //第一次提交上传正面照片
            file_key: 'front',
            form_data: e.detail.value,
            success: function(res) {
              try {
                var data = JSON.parse(res.data)
              } catch (e) {
                var data = res.data
              }
              if (data.success) {
                //提交窗
                wx.showLoading({
                  title: _('提交中')
                })
                that.setData({
                  hash: data.data.hash
                })
                e.detail.value.hash = that.data.hash
                that.uploadfile({
                  file: that.data.tempFilePaths['back'], //第二次提交上传背面照片
                  file_key: 'back',
                  form_data: e.detail.value,
                  success: function(res) {
                    wx.hideLoading() //关闭提交窗
                    that.bindBackViewTap()
                  },
                  fail: function() {
                    wx.hideLoading() //关闭提交窗
                  }
                })
              }
              wx.showToast({ //显示返回结果
                title: data.message,
                icon: 'none',
                duration: 3000
              })
            }
          })
        } catch (e) {
          console.log(e)
        }
      }
    } else {
      wx.showToast({ //显示错误提示
        title: checked_result.message,
        icon: 'none',
        duration: 3000
      })
    }
  },
  /**
   * 处理地址删除
   */
  bindDeleteAddressTap: function(e) {
    let that = this
    let hash = e.currentTarget.dataset.hash
    let openid = app.globalData.openid
    //提交窗
    wx.showLoading({
      title: _('提交中')
    })
    //删除地址请求
    utils.requestQueryByPost({
      url: '/mobile/wechat/deleteAddress',
      form_data: {
        hash: hash,
        openid: openid
      },
      success: function(res) {
        if (res.data.success) {
          that.bindBackViewTap()
        }
        wx.showToast({ //显示返回结果
          title: res.data.message,
          icon: 'none',
          duration: 3000
        })
      },
      complete: function() {
        wx.hideLoading() //关闭提交窗
      }
    })
  },
  /**
   * 上传身份证，需要被调用两次
   */
  uploadfile: function(e) {
    var that = this
    console.log(e.form_data)
    //如果身份证图片文件名中包含域名，说明未使用本地图片；如果使用本地图片才去调用文件上传接口；不使用本地图片的时候，则使用数据更新文件接口
    if (e.file.indexOf('www.eqiyu.com') >= 0) {
      utils.requestQueryByPost({
        url: '/mobile/wechat/uploadFiles',
        form_data: e.form_data,
        success: e.success,
        complete: e.complete
      });
    } else {
      console.log(e)
      if (e.file == "") {
        wx.showToast({
          title: _('填写身份证号的同时需要上传身份证照片！'),
          icon: 'none',
          duration: 3000
        })
      } else {
        wx.uploadFile({
          url: app.globalData.server_url + '/mobile/wechat/uploadFiles',
          filePath: e.file,
          name: e.file_key,
          formData: e.form_data,
          dataType: 'json',
          header: {
            'Content-Type': 'multipart/form-data'
          },
          success: e.success,
          complete: e.complete
        })
      }
    }
  },
  /**
   * 检查提交表单的内容格式是否正确
   */
  checkUpdateData: function(data) {
    var return_value = {
      is_checked: true,
      message: ''
    }

    //校验项目是否为空
    return_value.is_checked = (data.value.recipient_name == '') ? false : true
    return_value.is_checked = (data.value.recipient_telephone == '') ? false : true
    return_value.is_checked = (data.value.recipient_province_text == '') ? false : true
    return_value.is_checked = (data.value.recipient_city_text == '') ? false : true
    return_value.is_checked = (data.value.recipient_district_text == '') ? false : true
    return_value.is_checked = (data.value.recipient_street == '') ? false : true
    //return_value.is_checked = (data.value.recipient_id == '') ? false : true

    //校验为空的情况
    if (!return_value.is_checked) {
      return_value.message = _('提交的地址信息数据不能为空！')
      return return_value
    }

    //校验手机号格式
    if (!this.validateMobile(data.value.recipient_telephone)) {
      return_value.is_checked = false
      return_value.message = _('联系方式手机号码输入不合法，请重新输入!')
      return return_value
    }

    //如果存在身份照片的情况下要校验身份证号码输入格式
    if (data.value.recipient_id || this.data.hasAddressFront || this.data.hasAddressBack || this.data.tempFilePaths['front'] || this.data.tempFilePaths['back']) {
      if (!this.validateIdCard(data.value.recipient_id)) {
        return_value.is_checked = false
        return_value.message = '身份证格式不正确!'
        return return_value
      }

      //校验身份证正面照片
      if (this.data.tempFilePaths['front'] == '') {
        return_value.is_checked = false
        return_value.message = '身份证照片正面未选择!'
        return return_value
      }

      //校验身份证背面照片
      if (this.data.tempFilePaths['back'] == '') {
        return_value.is_checked = false
        return_value.message = '身份证照片背面未选择!'
        return return_value
      }
    }

    return return_value
  },
  /**
   * 校验手机号格式是否正确
   */
  validateMobile: function(mobile) {
    var reg = /^1[34578]\d{9}$/;
    if (reg.test(mobile) === false) {
      return false;
    }
    return true;
  },
  /**
   * 校验身份证号码是否正确
   */
  validateIdCard: function(idCard) {
    //15位和18位身份证号码的正则表达式
    var regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
    //如果通过该验证，说明身份证格式正确，但准确性还需计算
    if (regIdCard.test(idCard)) {
      if (idCard.length == 18) {
        var idCardWi = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); //将前17位加权因子保存在数组里
        var idCardY = new Array(1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
        var idCardWiSum = 0; //用来保存前17位各自乖以加权因子后的总和
        for (var i = 0; i < 17; i++) {
          idCardWiSum += idCard.substring(i, i + 1) * idCardWi[i];
        }
        var idCardMod = idCardWiSum % 11; //计算出校验码所在数组的位置
        var idCardLast = idCard.substring(17); //得到最后一位身份证号码
        //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
        if (idCardMod == 2) {
          if (idCardLast == "X" || idCardLast == "x") {
            return true;
            //alert("恭喜通过验证啦！");
          } else {
            return false;
            //alert("身份证号码错误！");
          }
        } else {
          //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
          if (idCardLast == idCardY[idCardMod]) {
            //alert("恭喜通过验证啦！");
            return true;
          } else {
            return false;
            //alert("身份证号码错误！");
          }
        }
      }
    } else {
      //alert("身份证格式不正确!");
      return false;
    }
  }
})