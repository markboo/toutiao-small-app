const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
   data: {
      orders: {},
      order_staus: {},
      filter: ''
   },
   onLoad: function(option) {

     this.setData({
       _t: utils._t(), //翻译
     });

      var filter = option.filter
      this.loadOrdersList(filter)
   },
   loadOrdersList: function(filter) {
      wx.showLoading({
         title: _('加载中'),
         success: function() {}
      })
      var that = this;
      var openid = app.globalData.openid
      var user_id = app.globalData.user_id

      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetMainOrderByOpenid',
         form_data: {
            user_id: user_id,
            openid: openid,
            filter: filter
         },
         success: function(res) {
            if (res.data.success) {
               that.setData({
                  orders: res.data.data,
                  order_staus: app.globalData.orderStatus
               })
            }
         },
         complete: function() {
            wx.hideLoading()
         }
      })
   },
   bindOrderFilterTap: function(e) {
      var filter = e.currentTarget.dataset.filter
      this.loadOrdersList(filter)
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   },
   bindPaymentViewTap: function(e) {
      var that = this
      var openid = app.globalData.openid
      var orderid = e.currentTarget.dataset.orderid
      var total_price = e.currentTarget.dataset.price

      wx.showLoading({
         title: _('支付处理中')
      })

      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxRePayment',
         form_data: {
            openid: openid,
            main_order_code: orderid
         },
         success: function(res) {
            var respones = res.data.data;
            wx.requestPayment({
               timeStamp: respones.timeStamp,
               nonceStr: respones.nonceStr,
               package: respones.package,
               signType: respones.signType,
               paySign: respones.paySign,
               success: function(r) {
                  app.globalData.paymentData.pay_status = true
                  app.globalData.paymentData.message = _('支付成功')
                  app.globalData.paymentData.pay_amount = total_price
                  app.globalData.paymentData.order_code = orderid
                  wx.redirectTo({
                     url: '../payment/payment'
                  })
               },
               fail: function(res) {
                  app.globalData.paymentData.pay_status = false
                  if (res.errMsg == "requestPayment:fail cancel") {
                     app.globalData.paymentData.message = _('用户取消支付，请重新支付！')
                  } else {
                     app.globalData.paymentData.message = _('支付失败，请继续支付！')
                  }
                  wx.redirectTo({
                     url: '../payment/payment'
                  })
               },
               complete: function(res) {
                  console.log(res)
                  wx.hideLoading()
               }
            })
         }
      })
   },
   bindReCartViewTap: function(e) {
      var that = this;
      var orderid = e.currentTarget.dataset.orderid
      var openid = app.globalData.openid
      var user_id = app.globalData.user_id
      wx.showLoading({
         title: _('提交中'),
         success: function() {}
      })
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxBuyAgain',
         form_data: {
            user_id: user_id,
            openid: openid,
            main_order_code: orderid
         },
         success: function(res) {
            if (res.data.success) {
               wx.switchTab({
                  url: '../cart/cart'
               })
            } else {
               wx.showToast({ //显示错误提示
                  title: res.data.message,
                  icon: 'none',
                  duration: 2000
               })
            }
         },
         fail: function() {
            wx.showToast({
               title: res,
               icon: 'none',
               duration: 2000
            })
         },
         complete: function() {
            wx.hideLoading()
         }
      })
   },
   bindOrderViewTap: function(e) {
      let orderid = e.currentTarget.dataset.orderid
      wx.navigateTo({
         url: '../order/order?orderid=' + orderid
      })
   }
})