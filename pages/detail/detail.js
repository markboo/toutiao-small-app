//detail.js
//获取应用实例
const app = getApp()
const utils = require('../../utils/util.js')
const common = require('../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    indicatorDots: true,
    vertical: false,
    autoplay: false,
    circular: true,
    interval: 2000,
    duration: 500,
    previousMargin: 0,
    nextMargin: 0,
    commendlist: [],
    goodsnumber: 1,
    showDetailImage: false,
    detail_data: {
      goods_price: 0.00
    },
    shop_data: {},
    goods_desc_picture: {},
    bottom_cart_button: false,
    valid_time: 0,
    show_time: '',
    data_success: false,
    portrait_temp: '',
    is_share: false,
    pictrue_index: 0,
    show_flag: false,
    page: 1,
    show_loading: true,
    current_page: 1,
    total_page: 1,
    no_domino_page: true,
    share_image: '',
    hasNoData: false, //如果为检索到数据则显示,
    is_sharer: false,
    isShow: false
  },
  onLoad: function (options) {

    tt.showLoading({
      title: _('加载中'),
      mask: 'true'
    })

    this.setData({
      _t: utils._t(), //翻译
      language: wx.getStorageSync('Language')
    });

    var that = this
    let user_id = ''
    let share_user_id = ''
    let hash = ''
    let domino = ''
    var openid = app.globalData.openid

    //如果来源于二维码扫码url参数解析
    if (options.q !== undefined) {
      var scan_url = decodeURIComponent(options.q);
      var result = utils.parseURL(scan_url);
      user_id = result.s
      share_user_id = result.r
      hash = result.h
      domino = result.d
    } else { //否则来源于页面迁移和分享点击
      user_id = options.s
      share_user_id = options.r
      hash = options.h
      domino = options.d
    }

    //options.scene = true //测试扫小程序码使用

    //判断详情页面来自接龙页面商品
    if (domino == 'domino') {
      that.setData({
        no_domino_page: false
      })
      wx.hideShareMenu({}) //来源于接龙商品详情，关闭分享菜单
    }

    if (share_user_id !== null && share_user_id !== undefined && share_user_id !== '') {
      app.globalData.share_user_id = share_user_id
    } else {
      share_user_id = app.globalData.share_user_id
    }

    console.log('hash:' + hash)
    console.log(options)
    console.log(app.globalData.share_user_id)

    //设置是否显示授权图层
    that.setData({
      is_sharer: app.globalData.is_sharer
    })

    //判断是否来自于扫描小程序码
    if (options.scene) {
      console.log('小程序码')
      let share_hash = decodeURIComponent(options.scene)
      console.log('share_hash:' + share_hash)
      //share_hash = '979715b390413a905f2d0e77cc1c564e' //测试扫小程序码使用
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxGetHashAndShareIdByShareHash',
        form_data: {
          share_hash: share_hash
        },
        success: function (res) {
          if (res.data.success) {
            user_id = res.data.data.user_id
            if (user_id !== null && user_id !== undefined && user_id !== '') {
              app.globalData.user_id = user_id
            } else {
              user_id = app.globalData.user_id
            }
            share_user_id = res.data.data.share_user_id
            if (share_user_id !== null && share_user_id !== undefined && share_user_id !== '') {
              app.globalData.share_user_id = share_user_id
            }
            hash = res.data.data.goods_hash
            //初始化页面数据
            that.initData(user_id, openid, hash)
          } else {
            user_id = app.globalData.user_id
          }
        }
      })
    } else {
      //通过商品获取该商品的userid
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxGetUserIdByShareGoods',
        form_data: {
          hash: hash,
          share_user_id: share_user_id,
          openid: openid
        },
        success: function (res) {
          if (res.data.success) {
            user_id = res.data.data.user_id
            if (user_id !== null && user_id !== undefined && user_id !== '') {
              app.globalData.user_id = user_id
            } else {
              user_id = app.globalData.user_id
            }
            share_user_id = res.data.data.share_user_id
            if (share_user_id !== null && share_user_id !== undefined && share_user_id !== '') {
              app.globalData.share_user_id = share_user_id
            } else {
              share_user_id = app.globalData.share_user_id
            }
            //初始化页面数据
            that.initData(user_id, openid, hash)
          } else {
            user_id = app.globalData.user_id
          }
        }
      })
    }
  },
  initData: function (user_id, openid, hash) {
    var share_user_id = app.globalData.share_user_id
    var that = this
    //获取店铺信息
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetShopInfo',
      form_data: {
        user_id: user_id
      },
      success: function (res) {
        if (res.data.success) {
          that.setData({
            shop_data: res.data.data
          })
          if (!that.data.no_domino_page) {
            var shop_data = that.data.shop_data
            shop_data.shop_show = false
            that.setData({
              shop_data: shop_data
            })
          }
        }
      }
    })
    //获取商品详情
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGoodsDetail',
      form_data: {
        user_id: user_id,
        hash: hash,
        share_user_id: share_user_id,
        is_sharer: that.data.is_sharer,
        openid: openid
      },
      success: function (res) {
        if (res.data.success) {
          if (res.data.data.is_group_buy) {
            var timestamp = Date.parse(new Date()) / 1000
            //团购商品有效时间
            var valid_time = res.data.data.group_goods_valid_datetime
            that.setData({
              detail_data: res.data.data,
              valid_time: valid_time,
              hasNoData: false,
              show_flag: true,
              is_sharer: app.globalData.is_sharer
            })
            that.timer()
            setInterval(that.timer, 1000)
          } else {
            that.setData({
              detail_data: res.data.data,
              hasNoData: false,
              show_flag: true
            })
          }
          if (that.data.no_domino_page) {
            that.setData({
              data_success: res.data.success
            })
          }
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none',
            duration: 3000
          })
          that.setData({
            show_flag: true,
            hasNoData: true
          })
        }
      },
      complete: function () {
        that.getCommendGoodsList()
      }
    });
  },
  //计算团购结束倒计时
  timer: function (e) {
    var timestamp = Date.parse(new Date()) / 1000
    var valid_time = this.data.valid_time - timestamp
    var days = Math.floor(valid_time / (24 * 3600))
    var leave1 = valid_time % (24 * 3600) //计算天数后剩余的毫秒数 
    var hours = Math.floor(leave1 / (3600))
    var leave2 = leave1 % (3600) //计算小时数后剩余的毫秒数 
    var minutes = Math.floor(leave2 / (60))
    var leave3 = leave2 % (60) //计算分钟数后剩余的毫秒数 
    var seconds = Math.round(leave3)
    var out = days + "天" + hours + ":" + minutes + ":" + seconds
    this.setData({
      show_time: out
    })
  },
  //获取用户信息，如果未授权，显示授权图层
  bindGetUserInfo: function (e) {
    console.log(e)
    if (e.detail.userInfo) {
      //用户按了允许授权按钮 
      var that = this;
      //插入登录的用户的相关信息到数据库 
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxCheckWeixinUserInfo',
        form_data: {
          openid: app.globalData.openid,
          avatarUrl: e.detail.userInfo.avatarUrl,
          city: e.detail.userInfo.city,
          country: e.detail.userInfo.country,
          gender: e.detail.userInfo.gender,
          language: e.detail.userInfo.language,
          nickName: e.detail.userInfo.nickName,
          province: e.detail.userInfo.province,
          unionid: app.globalData.unionid
        },
        success: function (res) {
          //从数据库获取用户信息 
          //that.queryUsreInfo();
          console.log("插入小程序登录用户信息成功！");
        }
      });

      //授权成功后关闭遮盖 
      // app.globalData.show_authorize = false
      that.setData({
        // show_authorize: false
      })
    } else {
      //用户按了拒绝按钮 
      wx.showModal({
        title: _('警告'),
        content: _('您点击了拒绝授权，将无法进入小程序，请授权之后再进入!!!'),
        showCancel: false,
        confirmText: _('返回授权'),
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击了“返回授权”')
          }
        }
      })
    }
  },
  //处理商品列表显示商品详情
  bindGoodsViewTap: function (e) {
    var hash = e.currentTarget.dataset.hash; //获取按钮设置的数据
    wx.navigateTo({
      url: '../detail/detail?h=' + hash
    })
  },
  //处理商品加入收藏夹
  bindAddWishTap: function (e) {
    var that = this
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid
    var hash = e.currentTarget.dataset.hash
    utils.requestQueryByPost({
      url: '/mobile/wechat/addWishlist',
      form_data: {
        user_id: user_id,
        hash: hash,
        openid: openid
      },
      success: function (res) {
        if (res.data.success) {
          wx.showToast({
            title: res.data.message,
            icon: 'success',
            duration: 2000
          })
        } else {
          wx.showToast({
            title: _('该商品已收藏过！'),
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  //页面回滚到最顶端
  bindToPageTapViewTap: function () {
    //窗口滚动到最顶端
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 200
    })
  },
  //处理加入购物车
  bindPlusCartViewTap: function (e) {
    this.setData({
      isShow: false
    })
    this.bindAddCartViewTap(e)
  },
  //处理直接购买
  bindDirectPayViewTap: function (e) {
    //获取选择的商品的hash的数组
    var hash = e.currentTarget.dataset.hash
    var openid = app.globalData.openid
    var that = this

    if (hash != '') {
      app.globalData.orderGoods = hash;
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxGetDefalutAddress',
        form_data: {
          openid: openid
        },
        success: function (res) {
          app.globalData.defalutAddress = res.data.data
          app.globalData.addressId = res.data.data.hash
          wx.navigateTo({
            url: '../confirm/confirm?c=' + that.data.goodsnumber
          })
        }
      })
    }
  },
  //处理加入购物车
  bindAddCartViewTap: function (e) {
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var openid = app.globalData.openid
    var goods_hash = e.currentTarget.dataset.hash
    var goods_quantity = this.data.goodsnumber
    common.addGoodsToCart(user_id, share_user_id, openid, goods_hash, goods_quantity)
  },
  bindToCartViewTap: function () {
    wx.switchTab({
      url: '../cart/cart'
    })
  },
  bindToHomeTap: function () {
    wx.switchTab({
      url: '../index/index'
    })
  },
  bindBackViewTap: function () {
    wx.navigateBack({})
  },
  bindPrivacyViewTap: function () {
    wx.navigateTo({
      url: '../posts/posts?query=privacy'
    })
  },
  bindServicesViewTap: function () {
    wx.navigateTo({
      url: '../posts/posts?query=services'
    })
  },
  bindSafetyViewTap: function () {
    wx.navigateTo({
      url: '../safety/safety'
    })
  },
  //处理显示商品详情图
  bindShowDetailImageTap: function () {
    var that = this
    var erp_sku = this.data.detail_data.erp_sku
    var user_id = app.globalData.user_id
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGoodsDetailPicture',
      form_data: {
        user_id: user_id,
        erp_sku: erp_sku
      },
      success: function (res) {
        if (res.data.success) {
          that.setData({
            goods_desc_picture: res.data.data,
            showDetailImage: true
          })
        }
      }
    })
  },
  //处理团购商品
  bindGroupBuyViewTap: function (e) {
    var openid = app.globalData.openid
    var that = this
    var hash = this.data.detail_data.hash
    if (hash != '') {
      app.globalData.orderGoods = hash;
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxGetDefalutAddress',
        form_data: {
          openid: openid
        },
        success: function (res) {
          app.globalData.defalutAddress = res.data.data
          wx.navigateTo({
            url: '../confirm/confirm?g=true&p='
          })
        }
      })
    }
  },
  //处理商品数量减少
  numberMinusTap: function (e) {
    this.setData({
      goodsnumber: this.data.goodsnumber > 1 ? this.data.goodsnumber - 1 : 1
    })
  },
  //处理商品数量增加
  numberPlusTap: function (e) {
    this.setData({
      goodsnumber: this.data.goodsnumber * 1 + 1
    })
  },
  //处理分享商品
  onShareAppMessage: function (res) {
    if(this.data.detail_data.goods_picture[0])console.log(this.data.detail_data.goods_picture[0])
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    if (res.from === 'button') {
      // 来自页面内转发按钮
    }
    return {
      title: this.data.detail_data.goods_name,
      desc: this.data.detail_data.goods_name,
      path: '/pages/detail/detail?r=' + share_user_id + '&s=' + user_id + '&h=' + this.data.detail_data.hash,
      imageUrl: this.data.detail_data.goods_picture[0]
    }
  },
  //处理保存商品图片
  onWeixinFriends: function (e) {
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var that = this
    var hash = e.currentTarget.dataset.hash
    wx.showLoading({
      title: _('加载中'),
    })
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetWeixinFriendsImage',
      form_data: {
        hash: hash,
        user_id: user_id,
        share_user_id: share_user_id
      },
      success: function (res) {
        if (res.data.success) {
          that.setData({
            share_image: res.data.data.share_image,
            is_share: true
          })
          wx.hideLoading()
        }
      }
    })
  },
  //处理保存商品图片
  onSaveShareCode: function () {
    var that = this
    wx.showLoading({
      title: _('保存图片中'),
    })
    that.operaDownloadPicture()
  },
  //关闭图片保存图层
  onCloseShareCode: function (e) {
    var that = this
    that.setData({
      portrait_temp: '',
      is_share: false
    })
  },
  /**
   * 下载商品图片并调用处理函数去生成分享用图片
   */
  operaDownloadPicture: function () {
    var that = this
    var picture = that.data.detail_data.goods_picture[that.data.pictrue_index]
    if (typeof (picture) !== "undefined" && that.data.pictrue_index < that.data.detail_data.goods_picture.length) {
      wx.downloadFile({
        url: picture,
        success: function (res) {
          var imageFile = res.tempFilePath
          wx.saveImageToPhotosAlbum({
            filePath: imageFile,
            success: function (e) {
              that.setData({
                pictrue_index: that.data.pictrue_index + 1
              })
              if (that.data.pictrue_index < that.data.detail_data.goods_picture.length) {
                that.operaDownloadPicture() //这里使用了递归，修改代码要注意保护
              }
            }
          })
        }
      })
    }
    if (that.data.pictrue_index == that.data.detail_data.goods_picture.length - 1) {
      picture = that.data.share_image
      wx.downloadFile({
        url: picture,
        success: function (res) {
          var imageFile = res.tempFilePath
          wx.saveImageToPhotosAlbum({
            filePath: imageFile,
            success: function (e) {
              wx.hideLoading()
              wx.showToast({
                title: _('图片保存成功，请前往相册中查看！'),
                icon: 'none',
                duration: 3000
              })
            }
          })
        }
      })
    }
  },
  //处理屏幕下拉加载数据
  onReachBottom: function () {
    if (this.data.current_page < this.data.total_page) {
      var page = this.data.current_page + 1;
      this.setData({
        show_loading: true,
        page: page
      })
      this.getCommendGoodsList()
    }
  },
  //处理获取推荐商品列表
  getCommendGoodsList: function () {
    var that = this
    var openid = app.globalData.openid
    var goods_data = this.data.commendlist
    var share_user_id = app.globalData.share_user_id
    that.setData({
      is_sharer: app.globalData.is_sharer
    })

    var form_data = {
      user_id: app.globalData.user_id,
      openid: openid,
      share_user_id: share_user_id,
      page: that.data.page,
      is_sharer: that.data.is_sharer,
      is_commend: 1
    }

    if (that.data.detail_data.goods_name) form_data['search_key'] = that.data.detail_data.goods_name

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGoodsList',
      form_data: form_data,
      success: function (response) {
        if (response.data.success) {

          for (var append_index in response.data.data.goods_list) {
            goods_data.push(response.data.data.goods_list[append_index]);
          }

          that.setData({
            commendlist: goods_data,
            show_loading: false,
            current_page: response.data.data.current_page,
            total_page: response.data.data.total_page,
            total_items: response.data.data.total
          })
        }
        wx.hideLoading()
      },
      fail: function () {
        wx.hideLoading()
      }
    })
  },
  bindToChangeGoodsSpuTap: function () {
    this.setData({
      isShow: !this.data.isShow
    })
  },
  bindChageSelectGoodsSpuTap: function (e) {
    var hash = e.currentTarget.dataset.hash
    this.initData(app.globalData.user_id, app.globalData.openid, hash)
  },
  handleContact: function(e) {
    console.log(e)
  },
  bindGoodsNumberKeyInput: function(e) {
    this.setData({
      goodsnumber: e.detail.value
    })
  }
})