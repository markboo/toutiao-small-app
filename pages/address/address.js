//address.js
const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
   data: {
      address_list: {},
      address_hash: '',
      r: ''
   },
   onLoad: function(option) {

     this.setData({
       _t: utils._t(), //翻译
     });

      var r = (option.r !== undefined && option.r != 'undefined') ? option.r : ''
      if (r == 'profile') {
         this.setData({
            r: r
         })
      }
   },
   onShow: function() {
      this.loadAddressList()
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   },
   bindEditViewTap: function(e) {
      var addressid = e.currentTarget.dataset.addressid;
      wx.navigateTo({
         url: '../edit/edit?addressid=' + addressid
      })
   },
   bindSelectedAddressViewTap: function(e) {
      var addressid = e.currentTarget.dataset.addressid;
      app.globalData.addressId = addressid;
      if (this.data.r !== 'profile') {
         wx.navigateBack({})
      }
   },
   loadAddressList: function() {
      wx.showLoading({
         title: _('加载中'),
         success: function() {}
      })
      var that = this;
      var openid = app.globalData.openid
      utils.requestQueryByPost({
         url: '/mobile/wechat/addresslist',
         form_data: {
            openid: openid
         },
         success: function(res) {
            that.setData({
               address_list: res.data.data
            })
         },
         complete: function() {
            wx.hideLoading()
         }
      })
   }
})