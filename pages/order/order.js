const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
  data: {
    order: {},
    orderid: '',
    show_flag: false,
    no_customer: true
  },
  onLoad: function (option) {

    this.setData({
      _t: utils._t(), //翻译
    });

    tt.showLoading({
      title: _('加载中')
    })
    var that = this;
    var openid = app.globalData.openid
    var user_id = app.globalData.user_id
    if (option == undefined) {
      var orderid = app.globalData.order_id
    } else {
      var orderid = option.orderid
    }
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetOrderDetail',
      form_data: {
        openid: openid,
        main_order_code: orderid
      },
      success: function (res) {
        console.log(res)
        if (res.data.success) {
          that.setData({
            order: res.data.data,
            orderid: orderid,
            show_flag: true
          })
        }
      },
      complete: function () {
        tt.hideLoading()
      }
    })
  },
  bindBackViewTap: function () {
    tt.navigateBack({})
  },
  bindPaymentViewTap: function () {
    var that = this
    var openid = app.globalData.openid
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var orderid = app.globalData.order_id
      //调用支付并生成订单返回签名，用于调取微信支付
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxToutiaoRePayment',
        form_data: {
          openid: openid,
          user_id: user_id,
          main_order_code: that.data.orderid,
          order_code: that.data.order.order_code
        },
        success: function (response) {
          console.log(response.data.data)
          tt.pay({
            orderInfo: response.data.data,
            service: 1,
            getOrderStatus(res) {
              console.log(res)
              let { out_order_no } = res;
              return new Promise(function (resolve, reject) {
                console.log('调取检查支付接口！')
                console.log(out_order_no)
                //商户前端根据 out_order_no 请求商户后端查询微信支付订单状态
                utils.requestQueryByPost({
                  url: '/mobile/wechat/ajaxCheckPayStatus',
                  form_data: {
                    openid: openid,
                    user_id: user_id,
                    pay_order_code: out_order_no,
                    share_user_id: share_user_id
                  },
                  success: function (resp) {
                    console.log('调取检查支付接口成功！')
                    console.log(out_order_no)
                    console.log(resp)
                    console.log('----------------------')
                    if (resp.data.success) {
                      //商户后端查询的微信支付状态，通知收银台支付结果
                      resolve({ code: resp.data.data.pay_status });
                    } else {
                      resolve({ code: 2 });
                    }
                  },
                  fail(err) {
                    console.log('调取检查支付接口失败！')
                    console.log(err)
                    reject(err);
                  }
                });
              });
            },
            success(res) {
              console.log('支付返回状态！')
              console.log(res)
              if (res.code == 0) { //支付成功
                // 支付成功处理逻辑，只有res.code=0时，才表示支付成功
                // 但是最终状态要以商户后端结果为准
                app.globalData.paymentData.pay_status = true
                app.globalData.paymentData.message = '支付成功'
              } else {
                if (res.code == 1) {
                  app.globalData.paymentData.message = '支付超时，请重新支付！'
                }
                if (res.code == 2) {
                  app.globalData.paymentData.message = '支付失败，请重新支付！'
                }
                if (res.code == 3) {
                  app.globalData.paymentData.message = '支付关闭，请重新支付！'
                }
                if (res.code == 4) {
                  app.globalData.paymentData.message = '取消支付，请重新支付！'
                }
                if (res.code == 9) {
                  app.globalData.paymentData.message = '订单状态未知/未支付，请继续支付！'
                }
                app.globalData.paymentData.pay_status = false
              }
              console.log(app.globalData.paymentData.message)
              app.globalData.paymentData.pay_amount = response.data.data.total_price
              app.globalData.paymentData.order_code = response.data.data.order_code

              tt.redirectTo({
                url: '/pages/payment/payment'
              })
            },
            fail(res) {
              // 调起收银台失败处理逻辑
              console.log('调起收银台失败')
              console.log(res)
              tt.showToast({ //发生错误显示返回结果
                title: '调起收银台失败',
                icon: 'none',
                duration: 2000
              })
            }
          });
        },
        fail: function (response) { },
        complete: function (response) {
          tt.hideLoading()
        }
      })
    
  },
  bindToInformationTap: function (e) {
    let orderid = e.currentTarget.dataset.orderid
    let express_code = e.currentTarget.dataset.expresscode
    let express_number = e.currentTarget.dataset.expressnumber
    let express_name = e.currentTarget.dataset.expressname
    console.log(orderid)

    tt.navigateTo({
      url: '../informat/informat?orderid=' + orderid + '&express_code=' + express_code + '&express_number=' + express_number + '&express_name=' + express_name
    })
  },
  bindCancleOrderTap: function (e) {
    var orderid = e.currentTarget.dataset.orderid
    tt.navigateTo({
      url: '../cancel/cancel?orderid=' + orderid
    })
  },
  bindReCartViewTap: function (e) {
    var that = this;
    var orderid = this.data.orderid
    var openid = app.globalData.openid
    var user_id = app.globalData.user_id
    tt.showLoading({
      title: _('提交中')
    })
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxBuyAgain',
      form_data: {
        user_id: user_id,
        openid: openid,
        main_order_code: orderid
      },
      success: function (res) {
        if (res.data.success) {
          tt.switchTab({
            url: '../cart/cart'
          })
        } else {
          tt.showToast({ //显示错误提示
            title: res.data.message,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail: function () {
        tt.showToast({
          title: res,
          icon: 'none',
          duration: 2000
        })
      },
      complete: function () {
        tt.hideLoading()
      }
    })
  },
  onShow: function () {
    if (app.globalData.reload_flag) {
      this.setData({
        show_flag: false
      })
      app.globalData.reload_flag = false
      getCurrentPages()[getCurrentPages().length - 1].onLoad()
    }
  }
})