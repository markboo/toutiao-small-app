//cancel.js
const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
   data: {
      reason: {},
      orderid: '',
      reason_code: 0
   },
   onLoad: function(options) {

     this.setData({
       _t: utils._t(), //翻译
     });

      var orderid = options.orderid
      this.setData({
         reason: app.globalData.cancelReason,
         orderid: orderid
      })
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   },
   radioChange: function(e) {
      let eason_code = e.detail.value
      if (eason_code == 0) {
         wx.showToast({ //显示错误提示
            title: _('请选择原因！'),
            icon: 'none',
            duration: 3000
         })
      } else {
         this.setData({
            eason_code: eason_code
         })
      }
   },
   bindCancelOrderTap: function(e) {
      let orderid = this.data.orderid
      let openid = app.globalData.openid
      let user_id = app.globalData.user_id
      let eason_code = this.data.eason_code

      wx.showLoading({
        title: _('加载中'),
         success: function() {}
      })
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxOrderCancel',
         form_data: {
            user_id: user_id,
            openid: openid,
            main_order_code: orderid,
            reason_code: eason_code
         },
         success: function(res) {
            wx.hideLoading()
            if (res.data.success) {
               app.globalData.reload_flag = true
               app.globalData.order_id = orderid
               wx.navigateBack({})
            } else {
               wx.showToast({ //显示错误提示
                  title: res.data.message,
                  icon: 'none',
                  duration: 5000
               })
            }
         },
         fail: function() {
            wx.showToast({
               title: res,
               icon: 'none',
               duration: 5000
            })
         },
         complete: function() {
         }
      })
   }
})