const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
  data: {
    array: {},
    region: {},
    address_data: {},
  },
  onLoad: function(option) {

    this.setData({
      _t: utils._t(), //翻译
    });

    var that = this
    //如果不是编辑状态而是新建状态，则初始化地址选择框
    that.setData({
      region: ['...', '...', '...']
    })

    utils.requestQueryByPost({
      url: '/mobile/posts/ajaxGetPosts',
      form_data: {
        posts: 'member'
      },
      success: function(res) {
        if (res.data.success) {
          that.setData({
            member: res.data.data
          })
        }
      }
    })
  },
  //返回
  bindBackViewTap: function() {
    wx.navigateBack({})
  },
  //地址选择改变
  bindRegionChange: function(e) {
    this.setData({
      region: e.detail.value
    })
  },
  //选择图片
  bindChooseImage: function(e) {
    var that = this
    var picture = e.currentTarget.dataset.picture
    wx.chooseImage({
      count: 1,
      success: function(res) {
        that.data.tempFilePaths[picture] = res.tempFilePaths[0]
        that.setData({
          tempFilePaths: that.data.tempFilePaths,
          hasAddressFront: false,
          hasAddressBack: false
        })
      }
    })
  },
  /**
   * 处理提交保存地址
   */
  bindSaveVIPTap: function(e) {
    var that = this
    //新建地址的时候hash是空的
    e.detail.value.recipient_province_text = this.data.region[0]
    e.detail.value.recipient_city_text = this.data.region[1]
    e.detail.value.recipient_district_text = this.data.region[2]
    e.detail.value.openid = app.globalData.openid
    e.detail.value.user_id = app.globalData.user_id
    e.detail.value.share_user_id = app.globalData.share_user_id

    var checked_result = that.checkUpdateData(e.detail) //校验页面提交数据

    if (checked_result.is_checked) { //校验结果为：成功
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxSaveVIPInfo',
        form_data: e.detail.value,
        success: function(res) {
          wx.hideLoading() //关闭提交窗
          if (res.data.success) {
            wx.showToast({ //显示返回结果
              title: res.data.message,
              icon: 'none',
              duration: 1500,
              success: function() {
                var timer = setTimeout(function() {
                  clearTimeout(timer)
                  wx.reLaunch({
                    url: '../index/index',
                  })
                }, 1500)

              }
            })
          }
          wx.showToast({ //显示返回结果
            title: res.data.message,
            icon: 'none',
            duration: 3000
          })
        },
        fail: function() {
          wx.hideLoading() //关闭提交窗
        }
      })
    } else {
      wx.showToast({ //显示错误提示
        title: checked_result.message,
        icon: 'none',
        duration: 3000
      })
    }
  },
  /**
   * 检查提交表单的内容格式是否正确
   */
  checkUpdateData: function(data) {
    var return_value = {
      is_checked: true,
      message: ''
    }

    //校验项目是否为空
    return_value.is_checked = (data.value.vip_name == '') ? false : true
    return_value.is_checked = (data.value.vip_telephone == '') ? false : true
    return_value.is_checked = (data.value.recipient_province_text == '') ? false : true
    return_value.is_checked = (data.value.recipient_city_text == '') ? false : true
    return_value.is_checked = (data.value.recipient_district_text == '') ? false : true
    return_value.is_checked = (data.value.vip_street == '') ? false : true

    //校验为空的情况
    if (!return_value.is_checked) {
      return_value.message = _('提交的地址信息数据不能为空！')
      return return_value
    }

    //校验手机号格式
    if (!this.validateMobile(data.value.vip_telephone)) {
      return_value.is_checked = false
      return_value.message = _('联系方式手机号码输入不合法，请重新输入!')
      return return_value
    }
    return return_value
  },
  /**
   * 校验手机号格式是否正确
   */
  validateMobile: function(mobile) {
    var reg = /^1[34578]\d{9}$/;
    if (reg.test(mobile) === false) {
      return false;
    }
    return true;
  },
})