//category.js
const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
   data: {
      categorys: {},
      small_category: {},
      current_category: ''
   },
   onLoad: function() {

     this.setData({
       _t: utils._t(), //翻译
     });

      var that = this
      var user_id = app.globalData.user_id
      
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetGoodsCategory',
         form_data: {
            user_id: user_id
         },
         success: function(res) {
            that.setData({
               categorys: res.data.data
            })
            if (that.data.current_category == '') {
               if (function() {
                     try {
                        return that.data.categorys[0].category_id, true
                     } catch (e) {}
                  }()) {
                  that.setData({
                     current_category: that.data.categorys[0].category_id
                  })
                  that.getSmaillCategoryList(that.data.categorys[0].category_id)
               }
            }
         }
      });
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   },
   bindSelectCategoryTap: function(e) {
      var category_id = e.currentTarget.dataset.category
      this.getSmaillCategoryList(category_id)
   },
   bindSelectSmallCategoryTap: function(e) {
      var category_id = e.currentTarget.dataset.category
      wx.navigateTo({
         url: '../list/list?category_little=' + category_id
      });
   },
   getSmaillCategoryList: function(category_id) {
      var that = this
      var user_id = app.globalData.user_id
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetGoodsSmallCategory',
         form_data: {
            user_id: user_id,
            category_id: category_id
         },
         success: function(res) {
            that.setData({
               small_category: res.data.data,
               current_category: category_id
            })
         }
      });
   }
})