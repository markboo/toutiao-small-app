//confirm.js
const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
  data: {
    address: {},
    goods_hash: '',
    order_data: {},
    post_fee: 0.00,
    post_template: {},
    total_price: 0.00,
    goods_list: {},
    error_message: '',
    is_group_buy: false,
    parent_main_order_code: '',
    goodsnumber: 1
  },
  onLoad: function (options) {

    this.setData({
      _t: utils._t(), //翻译
    });

    var that = this
    var goods_hash = app.globalData.orderGoods
    var goods_hash_number = app.globalData.orderGoodsNumber
    var openid = app.globalData.openid
    var user_id = app.globalData.user_id
    var is_group_buy = false
    var parent_main_order_code = ''
    var goodsnumber = 1

    tt.showLoading({
      title: _('加载中')
    })

    //通过请求获取参数
    if (options.g != '' && options.g !== undefined) {
      is_group_buy = options.g
      parent_main_order_code = options.p
    }

    if (options.c != '' && options.c !== undefined) {
      goodsnumber = options.c
      that.setData({
        goodsnumber: goodsnumber
      })
    }

    //判断如果页面不是来自接龙详情页面
    if (options.d != 'domino') {
      app.globalData.domino_id = ''
    } else {
      goodsnumber = goods_hash_number
    }

    //获取默认收货地址
    that.setData({
      address: app.globalData.defalutAddress,
      is_group_buy: is_group_buy,
      parent_main_order_code: parent_main_order_code
    })

    //去计算运费
    this.getPostFee(that.data.address.hash)

    //获取全部订单商品
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxOrderFill',
      form_data: {
        user_id: user_id,
        openid: openid,
        goods_hash: goods_hash,
        goods_number: goodsnumber,
        is_group_buy: is_group_buy,
        parent_main_order_code: parent_main_order_code
      },
      success: function (res) {
        var total = parseFloat(that.data.post_fee) + parseFloat(res.data.data.total_price) - parseFloat(res.data.data.off_amount)
        that.setData({
          order_data: res.data.data,
          total_price: total.toFixed(2)
        })
      },
      complete: function () {
        tt.hideLoading()
      }
    })
  },
  bindMakingUpViewTap: function (e) {
    var freight_id = e.currentTarget.dataset.id
    var url = '../list/list?freight_id=' + freight_id
    tt.navigateTo({
      url: url
    });
  },
  bindBackViewTap: function () {
    tt.navigateBack({})
  },
  bindPaymentViewTap: function () {
    var that = this
    var goods_hash = app.globalData.orderGoods
    var goods_hash_number = app.globalData.orderGoodsNumber
    var openid = app.globalData.openid
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var hash = that.data.address.hash
    var is_group_buy = this.data.is_group_buy
    var parent_main_order_code = this.data.parent_main_order_code
    var domino_id = app.globalData.domino_id
    var timestamp = (new Date()).valueOf();

    //判断如果页面不是来自接龙详情页面
    if (app.globalData.domino_id == '') {
      goods_hash_number = that.data.goodsnumber
    }

    if (hash === undefined) {
      tt.showToast({ //发生错误显示返回结果
        title: _('收货人未选择'),
        icon: 'none',
        duration: 2000
      })
    } else {
      tt.showLoading({
        title: _('调取支付中！')
      })

      //调用支付并生成订单返回签名，用于调取微信支付
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxCreateToutiaoOrderAndSignAndPay',
        form_data: {
          openid: openid,
          user_id: user_id,
          share_user_id: share_user_id,
          hash: hash,
          goods_hash: goods_hash,
          goods_number: goods_hash_number,
          is_group_buy: is_group_buy,
          parent_main_order_code: parent_main_order_code,
          domino_id: domino_id
        },
        success: function (response) {
          console.log(response.data.data)
          tt.pay({
            orderInfo: response.data.data,
            service: 1,
            getOrderStatus(res) {
              console.log(res)
              let { out_order_no } = res;
              return new Promise(function (resolve, reject) {
                console.log('调取检查支付接口！')
                console.log(out_order_no)
                //商户前端根据 out_order_no 请求商户后端查询微信支付订单状态
                utils.requestQueryByPost({
                  url: '/mobile/wechat/ajaxCheckPayStatus',
                  form_data: {
                    openid: openid,
                    user_id: user_id,
                    pay_order_code: out_order_no,
                    user_id: user_id,
                    share_user_id: share_user_id
                  },
                  success: function (resp) {
                    console.log('调取检查支付接口成功！')
                    console.log(out_order_no)
                    console.log(resp)
                    console.log('----------------------')
                    if (resp.data.success) {
                      //商户后端查询的微信支付状态，通知收银台支付结果
                      resolve({ code: resp.data.data.pay_status });
                    } else {
                      resolve({ code: 2 });
                    }
                  },
                  fail(err) {
                    console.log('调取检查支付接口失败！')
                    console.log(err)
                    reject(err);
                  }
                });
              });
            },
            success(res) {
              console.log('支付返回状态！')
              console.log(res)
              if (res.code == 0) { //支付成功
                // 支付成功处理逻辑，只有res.code=0时，才表示支付成功
                // 但是最终状态要以商户后端结果为准
                app.globalData.paymentData.pay_status = true
                app.globalData.paymentData.message = '支付成功'
              } else {
                if (res.code == 1) {
                  app.globalData.paymentData.message = '支付超时，请重新支付！'
                }
                if (res.code == 2) {
                  app.globalData.paymentData.message = '支付失败，请重新支付！'
                }
                if (res.code == 3) {
                  app.globalData.paymentData.message = '支付关闭，请重新支付！'
                }
                if (res.code == 4) {
                  app.globalData.paymentData.message = '取消支付，请重新支付！'
                }
                if (res.code == 9) {
                  app.globalData.paymentData.message = '订单状态未知/未支付，请继续支付！'
                }
                app.globalData.paymentData.pay_status = false
              }
              console.log(app.globalData.paymentData.message)
              app.globalData.paymentData.pay_amount = that.data.total_price
              app.globalData.paymentData.order_code = response.data.data.order_code
              tt.redirectTo({
                url: '/pages/payment/payment'
              })
            },
            fail(res) {
              // 调起收银台失败处理逻辑
              console.log('调起收银台失败')
              console.log(res)
              tt.showToast({ //发生错误显示返回结果
                title: '调起收银台失败',
                icon: 'none',
                duration: 2000
              })
            }
          });
        },
        fail: function (response) { },
        complete: function (response) {
          tt.hideLoading()
        }
      })
    }
  },
  bindAddressViewTap: function (e) {
    //获取按钮设置的类型
    var userid = e.currentTarget.dataset.userid;
    tt.navigateTo({
      url: '../address/address?userid=' + userid
    });
  },
  onShow: function () {
    var addressid = app.globalData.addressId;
    console.log('addressid: ' + addressid)
    if (addressid) {
      this.getAddressInfo(addressid)
      this.getPostFee(addressid)
      //返回地址ID用完需要清空
      app.globalData.addressId = null;
    }
  },
  getAddressInfo: function (hash) {
    //去服务器获取地址，并设定到页面
    var that = this
    //获取地址详情
    utils.requestQueryByPost({
      url: '/mobile/wechat/getAddressDetail',
      form_data: {
        hash: hash
      },
      success: function (res) {
        that.setData({
          address: res.data.data
        })
      }
    });
  },
  getPostFee: function (hash) {
    var that = this
    var goods_hash = app.globalData.orderGoods
    var goods_hash_number = app.globalData.orderGoodsNumber
    var openid = app.globalData.openid
    var user_id = app.globalData.user_id
    var is_group_buy = this.data.is_group_buy
    var parent_main_order_code = this.data.parent_main_order_code

    //获取计算运费
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetPostFee',
      form_data: {
        user_id: user_id,
        openid: openid,
        address_hash: hash,
        goods_hash: goods_hash,
        goods_number: that.data.goodsnumber,
        is_group_buy: is_group_buy,
        parent_main_order_code: parent_main_order_code
      },
      success: function (res) {
        if (res.data.success) {
          var total = parseFloat(that.data.order_data.total_price) + parseFloat(res.data.data.post_fee) - parseFloat(that.data.order_data.off_amount)
          that.setData({
            post_fee: res.data.data.post_fee,
            post_template: res.data.data.post_template,
            total_price: total.toFixed(2)
          })
        } else {
          var total = parseFloat(that.data.order_data.total_price)
          that.setData({
            post_fee: 0.00,
            total_price: total.toFixed(2)
          })
        }
      }
    })
  }
})