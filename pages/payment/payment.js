//payment.js
const app = getApp()
const utils = require('../../utils/util.js')
const common = require('../../utils/common.js')
const _ = utils._; //翻译函数

Page({
   data: {
      commendlist: [],
      success: true,
      pay_message: '',
      order_code: '',
      order_price: 0.00,
      pay_method: '微信支付',
      page: 1,
      show_loading: true,
      current_page: 1,
      total_page: 1,
      show_flag: false,
      is_sharer: false,
   },
   onLoad: function(option) {

     this.setData({
       _t: utils._t(), //翻译
       language: tt.getStorageSync('Language')
     });

      var that = this;
      var user_id = app.globalData.user_id
      var openid = app.globalData.openid

      that.setData({
         pay_message: app.globalData.paymentData.message,
         order_code: app.globalData.paymentData.order_code,
         order_price: app.globalData.paymentData.pay_amount,
         success: app.globalData.paymentData.pay_status
      })
      //获取推荐商品
      this.getCommendGoodsList()
   },
   //处理加入购物车
   bindAddCartViewTap: function (e) {
      var user_id = app.globalData.user_id
      var share_user_id = app.globalData.share_user_id
      var openid = app.globalData.openid
      var goods_hash = e.currentTarget.dataset.hash
      var goods_quantity = 1
      common.addGoodsToCart(user_id, share_user_id, openid, goods_hash, goods_quantity)
   },
   bindBackViewTap: function() {
      tt.navigateBack({})
   },
   bindOrderViewTap: function(e) {
      var that = this
      console.log(that.data)
      tt.redirectTo({
         url: '../order/order?orderid=' + that.data.order_code
      })
   },
   bindPrivacyViewTap: function () {
      tt.navigateTo({
         url: '../posts/posts?query=privacy'
      })
   },
   bindServicesViewTap: function () {
      tt.navigateTo({
         url: '../posts/posts?query=services'
      })
   },
   bindGoodsViewTap: function (e) {
      var hash = e.currentTarget.dataset.hash
      tt.navigateTo({
         url: '../detail/detail?h=' + hash
      })
   },
   onReachBottom: function () {
      if (this.data.current_page < this.data.total_page) {
         var page = this.data.current_page + 1;
         this.setData({
            show_loading: true,
            page: page
         })
         this.getCommendGoodsList()
      }
   },
   getCommendGoodsList: function () {
      var that = this
      var openid = app.globalData.openid
      var goods_data = this.data.commendlist
     var share_user_id = app.globalData.share_user_id

      that.setData({
         is_sharer: app.globalData.is_sharer
      })

      //获取推荐商品列表
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetGoodsList',
         form_data: {
            user_id: app.globalData.user_id,
            openid: openid,
           share_user_id: share_user_id,
           is_sharer: that.data.is_sharer,
            page: that.data.page,
            is_commend: 1
         },
         success: function (response) {
            if (response.data.success) {

               for (var append_index in response.data.data.goods_list) {
                  goods_data.push(response.data.data.goods_list[append_index]);
               }

               that.setData({
                  commendlist: goods_data,
                  show_loading: false,
                  current_page: response.data.data.current_page,
                  total_page: response.data.data.total_page,
                  total_items: response.data.data.total
               })
            }
            tt.hideLoading()
         }
      })
   }
})