//ad.js
const app = getApp()

Page({
   data: {
      logs: [],
      userInfo: {}
   },
   onLoad: function() {

   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   },
   bindSearchViewTap: function() {
      wx.redirectTo({
         url: '../list/list?search_key=SK'
      })
   }
})