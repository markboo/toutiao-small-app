//list.js
const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
   data: {
      page: 1,
      show_loading: true,
      dominos_list: [],
      total_items: 1,
      show_flag: false,
      shop_data: {},
      show_authorize: false
   },
   onLoad: function (options) {

     this.setData({
       _t: utils._t(), //翻译
     });

      var that = this
      let user_id = ''
      let hash = ''
      let share_user_id = ''
      var openid = app.globalData.openid

      //如果来源于二维码扫码url参数解析
      if (options.q !== undefined) {
         var scan_url = decodeURIComponent(options.q);
         var result = utils.parseURL(scan_url);
         user_id = result.s
         share_user_id = result.r
         hash = result.h
      } else { //否则来源于页面迁移和分享点击
         user_id = options.s
         share_user_id = options.r
         hash = options.h
      }

      app.thisAuthorizeCallback = rp => {
         that.setData({
            show_authorize: app.globalData.show_authorize
         })
      }
      app.globalData.user_id = user_id

      if (share_user_id !== null && share_user_id !== undefined && share_user_id !== '') {
         app.globalData.share_user_id = share_user_id
      }

      console.log('3:' + app.globalData.show_authorize)
     if (!app.globalData.show_authorize) {
       wx.showLoading({
         title: _('加载中'),
       })
     }
      this.searchGoodsList()
   },
   //事件处理函数
   bindDominoDetailViewTap: function (e) {
      var dominoid = e.currentTarget.dataset.dominoid //获取按钮设置的数据
      var user_id = app.globalData.user_id
      var share_user_id = app.globalData.share_user_id
      wx.navigateTo({
         url: '../dominodetail/dominodetail?r=' + share_user_id + '&d=' + dominoid + '&s=' + user_id
      })
   },
   bindBackViewTap: function () {
      wx.navigateBack({})
   },
   bindPrivacyViewTap: function () {
      wx.navigateTo({
         url: '../posts/posts?query=privacy'
      })
   },
   bindServicesViewTap: function () {
      wx.navigateTo({
         url: '../posts/posts?query=services'
      })
   },
   onShareAppMessage: function (res) {
      var user_id = app.globalData.user_id
      var share_user_id = app.globalData.share_user_id
      if (res.from === 'button') {
         // 来自页面内转发按钮
      }
      return {
         title: _('海外直邮，正品进口'),
         path: '/pages/dominodetail/dominodetail?r=' + share_user_id + '&d=' + dominoid + '&s=' + user_id
      }
   },
   searchGoodsList: function () {
      var that = this
      var user_id = app.globalData.user_id
      var openid = app.globalData.openid

      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetShopInfo',
         form_data: {
            user_id: user_id
         },
         success: function (res) {
            if (res.data.success) {
               that.setData({
                  shop_data: res.data.data
               })
            }
         }
      })
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetDominosList',
         form_data: {
            user_id: user_id,
            page: that.data.page,
            openid: openid
         },
         success: function (res) {
            if (res.data.success) {
               that.setData({
                  dominos_list: res.data.data.domino_list,
                  total_items: res.data.data.total_items,
                  show_flag: true
               })
            }
         },
         complete: function () {
            wx.hideLoading()
            that.setData({
               show_loading: false,
               show_flag: true
            })
         }
      });
   },
   bindGetUserInfo: function (e) {
      if (e.detail.userInfo) {
         //用户按了允许授权按钮 
         var that = this;
         //插入登录的用户的相关信息到数据库 
         utils.requestQueryByPost({
            url: '/mobile/wechat/ajaxCheckWeixinUserInfo',
            form_data: {
               openid: app.globalData.openid,
               avatarUrl: e.detail.userInfo.avatarUrl,
               city: e.detail.userInfo.city,
               country: e.detail.userInfo.country,
               gender: e.detail.userInfo.gender,
               language: e.detail.userInfo.language,
               nickName: e.detail.userInfo.nickName,
               province: e.detail.userInfo.province,
               unionid: app.globalData.unionid
            },
            success: function (res) {
               //从数据库获取用户信息 
               //that.queryUsreInfo();
               console.log("插入小程序登录用户信息成功！");
            }
         });
         //授权成功后关闭遮盖 
         app.globalData.show_authorize = false
         that.setData({
            show_authorize: false
         })
      } else {
         //用户按了拒绝按钮 
         wx.showModal({
            title: _('警告'),
            content: _('您点击了拒绝授权，将无法进入小程序，请授权之后再进入!!!'),
            showCancel: false,
            confirmText: _('返回授权'),
            success: function (res) {
               if (res.confirm) {
                  console.log('用户点击了“返回授权”')
               }
            }
         })
      }
   }
})