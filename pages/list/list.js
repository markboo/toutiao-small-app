//list.js
const app = getApp()
const utils = require('../../utils/util.js')
const common = require('../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    search_goods_list: [],
    goods_list: [],
    search_key: '',
    goods_type: '',
    category_large: '',
    category_little: '',
    brandid: '',
    search_page: 1,
    search_list: [],
    search_current_page: 1,
    search_total_page: 1,
    search_total_items: 1,
    page: 1,
    show_loading: true,
    list: [],
    current_page: 1,
    total_page: 1,
    commendlist: [],
    total_items: 1,
    show_filter: false,
    hidden: true,
    facet_fields: {},
    filter_height: 600,
    min_price: '',
    max_price: '',
    is_sharer: false,
  },
  onLoad: function(option) {

    this.setData({
      _t: utils._t(), //翻译
      language: wx.getStorageSync('Language')
    });

    var that = this
    var query = wx.createSelectorQuery();
    query.select('#title-bar').boundingClientRect()
    query.exec(function(res) {
      that.setData({
        filter_height: wx.getSystemInfoSync().windowHeight - res[0].height
      })
    })
    wx.showLoading({
      title: _('加载中'),
    })

    var that = this
    var sale = ''
    var goods_type = option.goodstype ? option.goodstype : ''
    var search_key = option.search_key ? option.search_key : ''
    var category_large = option.category_large ? option.category_large : ''
    var category_little = option.category_little ? option.category_little : ''
    var brandid = option.brandid ? option.brandid : ''
    var freight_id = option.freight_id ? option.freight_id : ''
    var share = option.share ? option.share : ''
    var user_id = option.s
    var share_user_id = option.r

    console.log(option)

    //判断远程请求中是否有user_id，如果有，使用url请求中的user_id
    if (user_id !== null && user_id !== undefined && user_id !== '') {
      app.globalData.user_id = user_id
    }

    //判断远程请求中是否有share_user_id，如果有，使用url请求中的share_user_id
    if (share_user_id !== null && share_user_id !== undefined && share_user_id !== '') {
      app.globalData.share_user_id = share_user_id
    }

    //处理聚划算商品，当商品类型等于5时
    if (goods_type == 5) {
      goods_type = ''
      sale = 1
    }

    this.setData({
      search_key: search_key,
      goods_type: goods_type,
      brandid: brandid,
      category_large: category_large,
      category_little: category_little,
      is_sharer: app.globalData.is_sharer,
      share: share,
      freight_id: freight_id
    })

    //检查是否获取openid，如果未获取，去获取openid
    if (app.globalData.openid == '') {
      app.thisOpenidCallback = res => {
        this.searchGoodsList(true)
      }
    } else {
      this.searchGoodsList(true)
    }
  },
  //分享商品列表
  onShareAppMessage: function(res) {
    var that = this
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid
    var share_user_id = app.globalData.share_user_id
    var share_image_list = [] //that.getShareImageList()
    var goodstype = that.data.goods_type
    var brandid = that.data.brandid
    var category_large = that.data.category_large
    var search_key = that.data.search_key
    var freight_id = that.data.freight_id
    var url = '/pages/list/list?r=' + share_user_id + '&s=' + user_id + '&goodstype=' + goodstype + '&brandid=' + brandid + '&category_large=' + category_large + '&search_key=' + search_key + '&freight_id=' + freight_id + '&share=' + true

    return {
      title: _('海外直邮，正品进口，商品列表分享！'),
      path: url,
      imageUrl: this.data.share_picture
    }
  },
  //事件处理函数
  bindGoodsViewTap: function(e) {
    var hash = e.currentTarget.dataset.hash; //获取按钮设置的数据
    wx.navigateTo({
      url: '../detail/detail?h=' + hash
    })
  },
  //处理加入购物车
  bindAddCartViewTap: function(e) {
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var openid = app.globalData.openid
    var goods_hash = e.currentTarget.dataset.hash
    var goods_quantity = 1
    common.addGoodsToCart(user_id, share_user_id, openid, goods_hash, goods_quantity)
  },
  bindSearchView: function(e) {
    var search_key = e.detail.value
    if (search_key != '') {
      wx.showLoading({
        title: '检索中',
      })
      this.setData({
        search_key: search_key,
        goods_type: '',
        category_large: '',
        category_little: '',
        brandid: '',
        search_page: 1,
        search_list: []
      })
      this.searchGoodsList(true)
    }
  },
  bindFilterSearchTap: function(e) {
    this.setData({
      show_filter: false,
      hidden: true,
      search_page: 1,
      search_list: []
    })
    wx.showLoading({
      title: _('检索中'),
    })
    this.searchGoodsList(false)
  },
  bindBackViewTap: function() {
    wx.navigateBack({})
  },
  bindPrivacyViewTap: function() {
    wx.navigateTo({
      url: '../posts/posts?query=privacy'
    })
  },
  bindServicesViewTap: function() {
    wx.navigateTo({
      url: '../posts/posts?query=services'
    })
  },
  bindToHomeTap: function () {
    wx.switchTab({
      url: '../index/index'
    })
  },
  getInputMinPirce: function(e) {
    var val = e.detail.value;
    this.setData({
      min_price: val
    })
  },
  getInputMaxPirce: function(e) {
    var val = e.detail.value;
    this.setData({
      max_price: val
    })
  },
  searchGoodsList: function(reload_filter) {
    var that = this
    var user_id = app.globalData.user_id
    var goods_data = this.data.search_list
    var openid = app.globalData.openid
    var filters = encodeURI(this.handleFilterData()) //对筛选内容进行url编码
    var share_user_id = app.globalData.share_user_id

    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGoodsList',
      form_data: {
        user_id: user_id,
        share_user_id: share_user_id,
        is_sharer: app.globalData.is_sharer,
        type: that.data.goods_type,
        page: that.data.search_page,
        search_key: that.data.search_key,
        brand_id: that.data.brandid,
        category_large: that.data.category_large,
        category_little: that.data.category_little,
        filters: filters,
        freight_id: that.data.freight_id,
        min_price: that.data.min_price,
        max_price: that.data.max_price,
        openid: openid
      },
      success: function(res) {
        if (res.data.success) {
          for (var append_index in res.data.data.goods_list) {
            goods_data.push(res.data.data.goods_list[append_index]);
          }
          that.setData({
            search_goods_list: res.data.data.goods_list,
            search_list: goods_data,
            search_current_page: res.data.data.current_page,
            search_total_page: res.data.data.total_page,
            search_total_items: res.data.data.total,
            is_sharer: app.globalData.is_sharer
          })
          if (reload_filter) {
            that.setData({
              facet_fields: that.formatFilterData(res.data.data.facet_fields),
            })
          }
          //当搜索商品数少于1页时，增加推荐商品
          if (res.data.data.total_page <= 1) {
            that.getCommendGoodsList()
          } else {
            var share_goods_picture_list = []
            for (var append_index in that.data.search_goods_list) {
              share_goods_picture_list.push(that.data.search_goods_list[append_index].goods_list_picture);
              if (append_index >= 5) break
            }
            //如果搜索商品数大于1页时，清除推荐商品设置
            that.setData({
              commendlist: [],
              show_loading: false,
              current_page: 1,
              total_page: 1,
              total_items: 1,
              share_image_list: share_goods_picture_list
            })
            that.setSharePicture()
          }
        }
      },
      complete: function() {
        wx.hideLoading()
        that.setData({
          show_loading: false
        })
      }
    });
  },
  setSharePicture() {
    var that = this
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid
    var share_user_id = app.globalData.share_user_id
    console.log(this.data.share_image_list)
    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetShareList',
      form_data: {
        user_id: app.globalData.user_id,
        openid: openid,
        share_image_list: this.data.share_image_list
      },
      success: function(response) {
        if (response.data.success) {
          that.setData({
            share_picture: response.data.data.image
          })
        }
      }
    })
  },
  /***
   *下拉刷新获取数据
   */
  onReachBottom: function() {
    if (this.data.hidden) {
      if (this.data.search_current_page < this.data.search_total_page) {
        var page = this.data.search_current_page + 1;
        this.setData({
          show_loading: true,
          search_page: page
        })
        this.searchGoodsList(false)
      }
      if (this.data.current_page < this.data.total_page) {
        var page = this.data.current_page + 1;
        this.setData({
          show_loading: true,
          page: page
        })
        this.getCommendGoodsList()
      }
    }
  },
  getCommendGoodsList: function() {
    var that = this
    var openid = app.globalData.openid
    var goods_data = this.data.commendlist
    var share_user_id = app.globalData.share_user_id

    that.setData({
      is_sharer: app.globalData.is_sharer
    })

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGoodsList',
      form_data: {
        user_id: app.globalData.user_id,
        openid: openid,
        share_user_id: share_user_id,
        is_sharer: that.data.is_sharer,
        page: that.data.page,
        is_commend: 1
      },
      success: function(response) {
        if (response.data.success) {
          for (var append_index in response.data.data.goods_list) {
            goods_data.push(response.data.data.goods_list[append_index]);
          }
          if (that.data.page <= 1) {
            var share_goods_picture_list = []
            for (var append_index in response.data.data.goods_list) {
              share_goods_picture_list.push(response.data.data.goods_list[append_index].goods_list_picture);
              if (append_index >= 5) break
            }
          }
          that.setData({
            commendlist: goods_data,
            show_loading: false,
            current_page: response.data.data.current_page,
            total_page: response.data.data.total_page,
            total_items: response.data.data.total,
            share_image_list: share_goods_picture_list
          })
          if (that.data.page <= 1) that.setSharePicture()
        }
      }
    });
  },
  /**
   * 点击筛选按钮 
   **/
  bindFilterTap: function() {
    if (this.data.show_filter) {
      this.setData({
        show_filter: false,
        hidden: true
      })
    } else {
      this.setData({
        show_filter: true,
        hidden: false
      })
    }
  },
  /**
   * 筛选项目清除 
   **/
  bindFilterClearTap: function(e) {
    this.setData({
      show_filter: false,
      hidden: true,
      search_page: 1,
      search_list: [],
      facet_fields: {},
      min_price: '',
      max_price: ''
    })
    this.searchGoodsList(true)
  },
  /*
   * 处理筛选点击
   */
  bindSelectOption: function(e) {
    var filter_index = e.currentTarget.dataset.filterindex
    var filter_item = e.currentTarget.dataset.filteritem
    var filters = this.data.facet_fields
    for (var item in filters) {
      for (var obj in filters[item]) {
        if (obj === filter_index) {
          if (filters[item][filter_index] != 'selected') filters[item][filter_index] = 'selected'
          else filters[item][filter_index] = ''
          this.setData({
            facet_fields: filters
          })
        }
      }
    }
  },
  /*
   * 处理过滤项目
   */
  handleFilterData: function() {
    var filters = this.data.facet_fields
    var new_filters = {}
    for (var item in filters) {
      new_filters[item] = {}
      for (var obj in filters[item]) {
        if (filters[item][obj] == 'selected') {
          new_filters[item][obj] = 'selected'
        }
      }
    }
    var data = JSON.stringify(new_filters)
    return data
  },
  /*
   * 格式化筛选器
   */
  formatFilterData: function(filters) {
    var new_filters = {}
    for (var item in filters) {
      if (JSON.stringify(filters[item]) != "{}") {
        new_filters[item] = {}
        for (var obj in filters[item]) {
          new_filters[item][obj] = filters[item][obj]
        }
      }
    }
    return new_filters
  }
})