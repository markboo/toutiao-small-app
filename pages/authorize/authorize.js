const app = getApp();
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
  data: {
    //判断小程序的API，回调，参数，组件等是否在当前版本可用。 
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  onLoad: function() {
    this.setData({
      _t: utils._t(), //翻译
    });

    var that = this;
    console.log(app.globalData.origin)
    //查看是否授权 
    wx.getSetting({
      success: function(res) {
        if (res.authSetting['scope.userInfo']) {
          wx.getUserInfo({
            success: function(res) {
              //从数据库获取用户信息 
              that.queryUsreInfo();
              //用户已经授权过
              /*
              wx.switchTab({
                 url: app.globalData.origin
              })
              */
              wx.navigateTo({
                url: app.globalData.origin
              })
            }
          });
        }
      }
    })
  },
  bindGetUserInfo: function(e) {
    if (e.detail.userInfo) {
      //用户按了允许授权按钮 
      var that = this;
      //插入登录的用户的相关信息到数据库 
      utils.requestQueryByPost({
        url: '/mobile/wechat/ajaxCheckWeixinUserInfo',
        form_data: {
          openid: app.globalData.openid,
          avatarUrl: e.detail.userInfo.avatarUrl,
          city: e.detail.userInfo.city,
          country: e.detail.userInfo.country,
          gender: e.detail.userInfo.gender,
          language: e.detail.userInfo.language,
          nickName: e.detail.userInfo.nickName,
          province: e.detail.userInfo.province,
          unionid: app.globalData.unionid
        },
        success: function(res) {
          //从数据库获取用户信息 
          that.queryUsreInfo();
          console.log("插入小程序登录用户信息成功！");
        }
      });

      //授权成功后，跳转进入小程序首页 
      /*
      wx.switchTab({
         url: app.globalData.origin
      })
      */
      wx.navigateTo({
        url: app.globalData.origin
      })

    } else {
      //用户按了拒绝按钮 
      wx.showModal({
        title: _('警告'),
        content: _('您点击了拒绝授权，将无法进入小程序，请授权之后再进入!!!'),
        showCancel: false,
        confirmText: _('返回授权'),
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击了“返回授权”')
          }
        }
      })
    }
  },
  //获取用户信息接口 
  queryUsreInfo: function() {
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetWeixinUserInfo',
      form_data: {
        openid: app.globalData.openid
      },
      success: function(res) {
        console.log(res.data);
        app.globalData.userInfo = res.data;
      }
    })
  },
})