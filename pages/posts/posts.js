//posts.js
const app = getApp()
const utils = require('../../utils/util.js')

Page({
   data: {
      data: {}
   },
   onLoad: function(options) {
      let q = options.query
      let that = this
      utils.requestQueryByPost({
         url: '/mobile/posts/ajaxGetPosts',
         form_data: {
            posts: q
         },
         success: function (res) {
            if (res.data.success) {
               that.setData({
                  data: res.data.data
               })
            }
         }
      })
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   }
})