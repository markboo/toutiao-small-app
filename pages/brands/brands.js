//brands.js
const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
   data: {
      large_category: [{
         id: "123456543",
         title: "面部护肤"
      }, {
         id: "123456545",
         title: "个人护理"
      }, {
         id: "123456543",
         title: "缤纷彩妆"
      }, {
         id: "123456543",
         title: "生活日用"
      }, {
         id: "123456543",
         title: "保健保险"
      }, {
         id: "123456543",
         title: "数码家电"
      }, {
         id: "123456543",
         title: "进口食品"
      }],
      small_category: {},
      seleced: "123456545"
   },
   onLoad: function() {

     this.setData({
       _t: utils._t(), //翻译
     });

      wx.showLoading({
        title: _('加载中'),
         success: function() {}
      })
      var that = this
      var user_id = app.globalData.user_id
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetBrands',
         form_data: {
            user_id: user_id
         },
         success: function(res) {
            that.setData({
               small_category: res.data.data
            })
         },
         complete: function() {
            wx.hideLoading()
         }
      });
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   },
   bindBrandGoodsViewTap: function(e) {
      var d = e.currentTarget.dataset
      var brandid = (d.brandid !== undefined && d.brandid != 'undefined') ? d.brandid : ''
      var url = '../list/list?brandid=' + brandid
      wx.navigateTo({
         url: url
      });
   }
})