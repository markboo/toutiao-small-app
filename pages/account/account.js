const app = getApp()
const utils = require('../../utils/util.js')
const common = require('../../utils/common.js')
const _ = utils._; //翻译函数

Page({
   data: {
      account_data: {
         "last_month_order_amount": "0.00",
         "this_month_order_amount": "0.00",
         "all_income": "0.00",
         "this_month_income": "0.00",
         "last_month_income": "0.00",
         "account_remainder": "0.00",
      }
   },
   onLoad: function() {

     this.setData({
       _t: utils._t(), //翻译
     });

      var that = this;
      var user_id = app.globalData.user_id
      var openid = app.globalData.openid
   },
   onShow: function() {
      this.initData()
   },
   initData: function() {

      var that = this;
      var openid = app.globalData.openid

      wx.showLoading({
         title: _('加载中'),
         success: function () { }
      })

      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetMyAccount',
         form_data: {
            share_user_id: app.globalData.share_user_id
         },
         success: function (res) {
            if (res.data.success) {
               that.setData({
                  account_data: res.data.data
               })
            }
            wx.hideLoading()
         },
         fail: function () {
            wx.hideLoading()
         }
      })
   },
   bindBackViewTap: function () {
      wx.navigateBack({})
   },
   bindToCashViewTap: function () {
      wx.navigateTo({
         url: '../account/cash/cash'
      })
   },
   bindToCashOutViewTap: function () {
      wx.navigateTo({
         url: '../account/cashout/cashout'
      })
   },
   bindToJournalViewTap: function () {
      wx.navigateTo({
         url: '../account/journal/journal'
      })
   },
   bindToIncomeViewTap: function () {
      wx.navigateTo({
         url: '../account/income/income'
      })
   }
})