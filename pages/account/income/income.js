const app = getApp()
const utils = require('../../../utils/util.js')
const common = require('../../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    date: '',
    month_income: {},
    month: 0
  },
  onLoad: function() {

    this.setData({
      _t: utils._t(), //翻译
    });

    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid

    that.getMonthData()
  },
  bindBackViewTap() {
    wx.navigateBack({})
  },
  bindDateChange(e) {
    var date = e.detail.value.split('-')
    var now_date = this.data.date.split('-')

    if (date.length > 1 && now_date.length > 1) {
      var month_count = (parseInt(date[0]) * 12 + parseInt(date[1])) - (parseInt(now_date[0]) * 12 + parseInt(now_date[1]))
      this.setData({
        date: e.detail.value,
        month: month_count
      })
      this.getMonthData()
    }
  },
  bindPageUPViewTap(e) {
    this.setData({
      month: this.data.month <= 0 ? this.data.month - 1 : 0
    })
    this.getMonthData()
  },
  bindPageDownViewTap(e) {
    this.setData({
      month: this.data.month < 0 ? this.data.month + 1 : 0
    })
    this.getMonthData()
  },
  getMonthData() {

    var that = this;

    wx.showLoading({
      title: _('加载中'),
      success: function() {}
    })

    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxMonthIncome',
      form_data: {
        share_user_id: app.globalData.share_user_id,
        month_count: this.data.month
      },
      success: function(res) {
        if (res.data.success) {
          that.setData({
            date: res.data.data.year_month,
            month_income: res.data.data.month_income
          })
        }
        wx.hideLoading()
      },
      fail: function() {
        wx.hideLoading()
      }
    })
  }
})