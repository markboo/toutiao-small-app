const app = getApp()
const utils = require('../../../utils/util.js')
const common = require('../../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    month_journal: {},
    date: '',
    month: 0
  },
  onLoad: function() {
    this.setData({
      _t: utils._t(), //翻译
    });
    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid

    that.getMonthData()
  },
  bindBackViewTap: function() {
    wx.navigateBack({})
  },
  bindDateChange(e) {
    var date = e.detail.value.split('-')
    var now_date = this.data.date.split('-')

    if (date.length > 1 && now_date.length > 1) {
      var month_count = (parseInt(date[0]) * 12 + parseInt(date[1])) - (parseInt(now_date[0]) * 12 + parseInt(now_date[1]))
      this.setData({
        date: e.detail.value,
        month: month_count
      })
      this.getMonthData()
    }
  },
  bindPageUPViewTap(e) {
    this.setData({
      month: this.data.month <= 0 ? this.data.month - 1 : 0
    })
    this.getMonthData()
  },
  bindPageDownViewTap(e) {
    this.setData({
      month: this.data.month < 0 ? this.data.month + 1 : 0
    })
    this.getMonthData()
  },
  getMonthData() {

    var that = this;

    wx.showLoading({
      title: _('加载中'),
      success: function() {}
    })

    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxMonthCashOutList',
      form_data: {
        share_user_id: app.globalData.share_user_id,
        month_count: this.data.month
      },
      success: function(res) {
        that.setData({
          date: res.data.data.year_month,
          month_journal: res.data.data.cash_out
        })
        wx.hideLoading()
      },
      fail: function() {
        wx.hideLoading()
      }
    })
  }
})