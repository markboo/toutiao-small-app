const app = getApp()
const utils = require('../../../utils/util.js')
const common = require('../../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    address_data: {},
    province: {},
    city: {},
  },
  onLoad: function() {

    this.setData({
      _t: utils._t(), //翻译
    });
    this.setData({
      province_value: _('请选择省份'),
      city_value: _('请选择城市')
    });
    wx.showLoading({
      title: _('加载中'),
      success: function() {}
    })
    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid

    that.initData()
  },
  initData: function() {

    var that = this;
    var openid = app.globalData.openid

    wx.showLoading({
      title: _('加载中'),
      success: function() {}
    })

    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetCashOutInfo',
      form_data: {
        share_user_id: app.globalData.share_user_id
      },
      success: function(res) {
        if (res.data.success) {
          that.setData({
            address_data: res.data.data,
            province_value: res.data.data.bank_province || _('请选择省份'),
            city_value: res.data.data.bank_city || _('请选择城市')
          })
        }
        wx.hideLoading()
      },
      fail: function() {
        wx.hideLoading()
      }
    })

    utils.requestQueryByPost({
      url: '/mobile/api/ajaxAddressProvinceArray',
      success: function(res) {
        if (res.data.success) {
          that.setData({
            province: res.data.data
          })
        }
        console.log(that.data.province)
        wx.hideLoading()
      },
      fail: function() {
        wx.hideLoading()
      }
    })

  },
  formSubmit: function(e) {
    var that = this;
    e.detail.value.share_user_id = app.globalData.share_user_id
    console.log('form发生了submit事件，携带数据为：', e.detail.value)

    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxSaveCashOutInfo',
      form_data: e.detail.value,
      success: function(res) {
        wx.hideLoading()
        wx.showToast({ //显示返回结果
          title: res.data.message,
          icon: 'none',
          duration: 3000
        })
      },
      fail: function() {
        wx.hideLoading()
      }
    })
  },
  bindBackViewTap: function() {
    wx.navigateBack({})
  },
  bindToJournalViewTap: function() {
    wx.navigateTo({
      url: '../../account/journal/journal'
    })
  },
  bindProvincePickerChange: function(e) {
    var that = this;
    that.setData({
      province_value: this.data.province[e.detail.value]
    })
    utils.requestQueryByPost({
      url: '/mobile/api/ajaxAddressCityArray',
      form_data: {
        province: that.data.province[e.detail.value]
      },
      success: function(res) {
        if (res.data.success) {
          that.setData({
            city: res.data.data
          })
        }
        console.log(that.data.city)
      }
    })
  },
  bindCityPickerChange: function(e) {
    this.setData({
      city_value: this.data.city[e.detail.value]
    })
  }
})