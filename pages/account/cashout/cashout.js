const app = getApp()
const utils = require('../../../utils/util.js')
const common = require('../../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    remainder: 0, //账户余额
    cash_out_amount: 0, //可提现金额：
    enchashing_money: 0, //提现中金额：
    apply_modify: false, //是否显示修改按钮：
    money: ''
  },
  onLoad: function() {

    this.setData({
      _t: utils._t(), //翻译
    });

    wx.showLoading({
      title: _('加载中'),
      success: function() {}
    })
    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid

    that.initData()
  },
  initData: function() {

    var that = this;
    var openid = app.globalData.openid

    wx.showLoading({
      title: _('加载中'),
      success: function() {}
    })

    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetEnchashing',
      form_data: {
        share_user_id: app.globalData.share_user_id
      },
      success: function(res) {
        if (res.data.success) {
          that.setData({
            remainder: res.data.data.remainder,
            cash_out_amount: res.data.data.cash_out_amount,
            enchashing_money: res.data.data.enchashing_money,
            apply_modify: res.data.data.apply_modify
          })
        }
        wx.hideLoading()
      },
      fail: function() {
        wx.hideLoading()
      }
    })

  },
  formSubmit: function(e) {
    var that = this;
    e.detail.value.share_user_id = app.globalData.share_user_id

    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxApplyCashOut',
      form_data: e.detail.value,
      success: function(res) {
        if (res.data.success) {
          that.setData({
            remainder: res.data.data.remainder,
            cash_out_amount: res.data.data.cash_out_amount,
            enchashing_money: res.data.data.enchashing_money,
            apply_modify: res.data.data.apply_modify,
            money: ''
          })
        }
        wx.hideLoading()
        wx.showToast({ //显示返回结果
          title: res.data.message,
          icon: 'none',
          duration: 3000
        })
      },
      fail: function() {
        wx.hideLoading()
      }
    })
  },
  bindBackViewTap: function() {
    wx.navigateBack({})
  },
  bindToJournalViewTap: function() {
    wx.navigateTo({
      url: '../../account/journal/journal'
    })
  },
  bindToCashViewTap: function() {
    wx.navigateTo({
      url: '../../account/cash/cash'
    })
  }
})