const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
   data: {
      data: {},
      orderid: '',
      express_number: '',
      express_name: ''
   },
   onLoad: function(option) {

     this.setData({
       _t: utils._t(), //翻译
     });

      let that = this;
      let openid = app.globalData.openid
      let user_id = app.globalData.user_id
      let orderid = option.orderid
      let express_code = option.express_code
      let express_number = option.express_number
      let express_name = option.express_name
      that.setData({
         orderid: orderid,
         express_code: express_code,
         express_number: express_number,
         express_name: express_name
      })

      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetExpressTrace',
         form_data: {
            user_id: user_id,
            openid: openid,
            express_code: express_code,
            express_number: express_number,
            erp_order_code: orderid
         },
         success: function (res) {
            if (res.data.success) {
               that.setData({
                  data: res.data.data
               })
            }
            console.log( that.data )
         }
      })
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   }
})