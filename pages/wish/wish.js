//wish.js
const app = getApp()
const utils = require('../../utils/util.js')
const common = require('../../utils/common.js')
const _ = utils._; //翻译函数

Page({
   data: {
      wishlist: {},
      commendlist: [],
      page: 1,
      show_loading: true,
      current_page: 1,
      total_page: 1,
      is_sharer: false,
   },
   onLoad: function(option) {

     this.setData({
       _t: utils._t(), //翻译
       language: wx.getStorageSync('Language')
     });

      wx.showLoading({
         title: _('加载中'),
         success: function() {}
      })
      var that = this;
      var user_id = app.globalData.user_id
      var openid = app.globalData.openid
      utils.requestQueryByPost({
         url: '/mobile/wechat/wishlist',
         form_data: {
            user_id: user_id,
            openid: openid
         },
         success: function(res) {
            that.setData({
               wishlist: res.data.data,
               is_sharer: app.globalData.is_sharer
            })
         },
         complete: function() {
            wx.hideLoading()
         }
      });

      this.getCommendGoodsList()
   },
   //事件处理函数
   bindGoodsViewTap: function(e) {
      var hash = e.currentTarget.dataset.hash; //获取按钮设置的数据
      wx.navigateTo({
         url: '../detail/detail?h=' + hash
      })
   },
   bindWishGoodsTrashTap: function(e) {
      var that = this;
      var user_id = app.globalData.user_id
      var openid = app.globalData.openid
      var hash = e.currentTarget.dataset.hash
      wx.showLoading({
         title: _('提交中'),
      })
      utils.requestQueryByPost({
         url: '/mobile/wechat/deleteWish',
         form_data: {
            hash: hash,
            user_id: user_id,
            openid: openid
         },
         success: function(res) {
            if (res.data.success) {
               that.deleteWishData(hash)
               wx.showToast({ //显示错误提示
                  title: res.data.message,
                  icon: 'none',
                  duration: 2000
               })
            }
         },
         complete: function() {
            wx.hideLoading()
         }
      })
   },
   //处理加入购物车
   bindAddCartViewTap: function (e) {
      var user_id = app.globalData.user_id
      var openid = app.globalData.openid
      var goods_hash = e.currentTarget.dataset.hash
      var goods_quantity = 1
      common.addGoodsToCart(user_id, openid, goods_hash, goods_quantity)
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   },
   bindPrivacyViewTap: function () {
      wx.navigateTo({
         url: '../posts/posts?query=privacy'
      })
   },
   bindServicesViewTap: function () {
      wx.navigateTo({
         url: '../posts/posts?query=services'
      })
   },
   deleteWishData: function(hash) {
      var wish_data = this.data.wishlist
      var index = wish_data.length;
      while (index--) {
         if (wish_data[index].hash == hash) {
            wish_data.splice(index, 1);
         }
      }
      this.setData({
         wishlist: wish_data
      })
   },
   onReachBottom: function () {
      if (this.data.current_page < this.data.total_page) {
         var page = this.data.current_page + 1;
         this.setData({
            show_loading: true,
            page: page
         })
         this.getCommendGoodsList()
      }
   },
   getCommendGoodsList: function () {
      var that = this
      var openid = app.globalData.openid
      var goods_data = this.data.commendlist
     var share_user_id = app.globalData.share_user_id

      that.setData({
         is_sharer: app.globalData.is_sharer
      })

      //获取推荐商品列表
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetGoodsList',
         form_data: {
            user_id: app.globalData.user_id,
            openid: openid,
           share_user_id: share_user_id,
           is_sharer: that.data.is_sharer,
            page: that.data.page,
            is_commend: 1
         },
         success: function (response) {
            if (response.data.success) {

               for (var append_index in response.data.data.goods_list) {
                  goods_data.push(response.data.data.goods_list[append_index]);
               }

               that.setData({
                  commendlist: goods_data,
                  show_loading: false,
                  current_page: response.data.data.current_page,
                  total_page: response.data.data.total_page,
                  total_items: response.data.data.total
               })
            }
         },
         complete: function () {
            wx.hideLoading()
         }
      })
   }
})