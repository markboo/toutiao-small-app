const app = getApp()
const utils = require('../../utils/util.js')
const common = require('../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    commendlist: [],
    userInfo: {},
    page: 1,
    show_loading: true,
    current_page: 1,
    total_page: 1,
    show_flag: false,
    is_sharer: false,
    return_my_share: false,
    my_account: false
  },
  onLoad: function() {

    this.setData({
      _t: utils._t(), //翻译
      language: wx.getStorageSync('Language'),
      is_sharer: app.globalData.is_sharer
    });
    
    wx.showLoading({
      title: _('加载中'),
      success: function() {}
    })
    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid

    this.getCommendGoodsList()

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    }

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetMyMenu',
      form_data: {
        is_sharer: app.globalData.is_sharer,
        openid: openid,
        share_user_id: app.globalData.share_user_id
      },
      success: function(response) {
        console.log(response)
        that.setData({
          return_my_share: response.data.data.return_my_share,
          my_account: response.data.data.my_account
        })
      }
    })
  },
  //处理加入购物车
  bindAddCartViewTap: function(e) {
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var openid = app.globalData.openid
    var goods_hash = e.currentTarget.dataset.hash
    var goods_quantity = 1
    common.addGoodsToCart(user_id, share_user_id, openid, goods_hash, goods_quantity)
  },
  bindGoodsViewTap: function(e) {
    var hash = e.currentTarget.dataset.hash
    wx.navigateTo({
      url: '../detail/detail?h=' + hash
    })
  },
  bindWishViewTap: function() {
    wx.navigateTo({
      url: '../wish/wish'
    });
  },
  bindHelpViewTap: function () {
    wx.navigateTo({
      url: '../guide/guide'
    });
  },
  bindOtherViewTap: function () {
    wx.navigateTo({
      url: '../profile/list/list'
    });
  },
  bindMyShopViewTap: function() {
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var openid = app.globalData.openid

    console.log('profile')
    console.log('user_id:' + user_id)
    console.log('share_user_id:' + share_user_id)
    console.log('openid:' + openid)
    console.log('profile')

    var url = '/mobile/wechat/ajaxGetMyShopInfo'

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: url,
      form_data: {
        openid: openid
      },
      success: function(response) {
        console.log(response)
        if (response.data.data.share_user_id != app.globalData.share_user_id) {
          app.globalData.user_id = response.data.data.user_id
          app.globalData.share_user_id = response.data.data.share_user_id
          app.globalData.index_is_reload = true
          wx.reLaunch({
            url: '../index/index',
          });
        }
      }
    })
  },
  bindAccountViewTap: function() {
    wx.navigateTo({
      url: '../account/account'
    });
  },
  bindOrdersViewTap: function(e) {
    var filter = e.currentTarget.dataset.filter
    wx.navigateTo({
      url: '../orders/orders?filter=' + filter
    });
  },
  bindAddressViewTap: function(e) {
    wx.navigateTo({
      url: '../address/address?r=profile'
    });
  },
  bindCategoryViewTap: function(e) {
    wx.navigateTo({
      url: '../category/category'
    })
  },
  switchLanguage: function() {
    if (utils.getLanguage() == 'zh_CN') {
      wx.setStorageSync('Language', 'jp'); // 利用本地缓存存放用户语言选项
    } else {
      wx.setStorageSync('Language', 'zh_CN');
    };
    wx.reLaunch({
      url: '../index/index',
    });
  },
  clearCache: function() {
    wx.reLaunch({
      url: '../index/index',
    });
  },
  getUserInfo: function(e) {
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  onReachBottom: function() {
    if (this.data.current_page < this.data.total_page) {
      var page = this.data.current_page + 1;
      this.setData({
        show_loading: true,
        page: page
      })
      this.getCommendGoodsList()
    }
  },
  getCommendGoodsList: function() {
    var that = this
    var openid = app.globalData.openid
    var goods_data = this.data.commendlist
    var share_user_id = app.globalData.share_user_id

    that.setData({
      is_sharer: app.globalData.is_sharer
    })

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGoodsList',
      form_data: {
        user_id: app.globalData.user_id,
        openid: openid,
        share_user_id: share_user_id,
        is_sharer: that.data.is_sharer,
        page: that.data.page,
        is_commend: 1
      },
      success: function(response) {
        if (response.data.success) {

          for (var append_index in response.data.data.goods_list) {
            goods_data.push(response.data.data.goods_list[append_index]);
          }

          that.setData({
            commendlist: goods_data,
            show_loading: false,
            current_page: response.data.data.current_page,
            total_page: response.data.data.total_page,
            total_items: response.data.data.total
          })
        }
        wx.hideLoading()
      }
    })
  }
})