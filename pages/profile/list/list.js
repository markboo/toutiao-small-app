const app = getApp()
const utils = require('../../../utils/util.js')
const _ = utils._; //翻译函数

Page({
  data: {
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    moive_list: [],
    userInfo: {}
  },
  onLoad: function () {
    this.setData({
      _t: utils._t(), //翻译
      language: wx.getStorageSync('Language')
    });

    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    }
  },
  //返回
  bindBackViewTap: function () {
    wx.navigateBack({})
  },
  bindPrivacyViewTap: function () {
    wx.navigateTo({
      url: '../../posts/posts?query=privacy'
    })
  },
  bindServicesViewTap: function () {
    wx.navigateTo({
      url: '../../posts/posts?query=services'
    })
  },
  bindSafetyViewTap: function () {
    wx.navigateTo({
      url: '../../posts/posts?query=safety'
    })
  },
  bindAboutViewTap: function () {
    wx.navigateTo({
      url: '../../posts/posts?query=about'
    })
  }
})