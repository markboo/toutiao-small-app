const app = getApp()
const utils = require('../../utils/util.js')
const common = require('../../utils/common.js')
const _ = utils._; //翻译函数

Page({
  data: {
    commendlist: [],
    goodscount: 1,
    goodsprice: 0,
    allselected: true,
    goods_total: 1,
    selected_count: 0,
    page: 1,
    show_loading: true,
    current_page: 1,
    total_page: 1,
    show_flag: false,
    is_sharer: false,
  },
  onLoad: function(option) {
    this.setData({
      _t: utils._t(), //翻译
      language: wx.getStorageSync('Language')
    });
    var that = this;
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid
    //获取推荐商品
    this.getCommendGoodsList()
  },
  //前往商品详情页面
  bindGoodsViewTap: function(e) {
    var hash = e.currentTarget.dataset.hash
    wx.navigateTo({
      url: '../detail/detail?h=' + hash
    })
  },
  //处理加入购物车
  bindAddCartViewTap: function(e) {
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var openid = app.globalData.openid
    var goods_hash = e.currentTarget.dataset.hash
    var goods_quantity = 1
    common.addGoodsToCart(user_id, share_user_id, openid, goods_hash, goods_quantity)
  },
  /**
   * 提交商品确认订单
   */
  bindConfirmViewTap: function() {
    var any_checked = false
    var openid = app.globalData.openid
    var that = this
    //检查是否有选择商品
    this.data.data.forEach(function(value, index, array) {
      if (array[index].checked) any_checked = true
    })
    //如果有选择的商品，则前往订单确认页面，没有则弹出提示
    if (any_checked) {
      var off_checked = false
      this.data.data.forEach(function(value, index, array) {
        if (array[index].offshelves && array[index].checked) {
          off_checked = true
        }
      })
      if (off_checked) {
        wx.showToast({
          title: _('选择的商品中有下架商品，不能提交购物车！'),
          icon: 'none'
        })
        return false
      }
      //获取选择的商品的hash的数组
      var hash = that.getSelectedGoodsFormCart()
      if (hash != '') {
        app.globalData.orderGoods = hash;
        utils.requestQueryByPost({
          url: '/mobile/wechat/ajaxGetDefalutAddress',
          form_data: {
            openid: openid
          },
          success: function(res) {
            app.globalData.defalutAddress = res.data.data
            app.globalData.addressId = res.data.data.hash
            wx.navigateTo({
              url: '../confirm/confirm'
            })
          }
        })
      }
    } else {
      wx.showToast({
        title: _('您还没有选择商品！'),
        icon: 'none'
      })
    }
  },
  bindPrivacyViewTap: function() {
    wx.navigateTo({
      url: '../posts/posts?query=privacy'
    })
  },
  bindServicesViewTap: function() {
    wx.navigateTo({
      url: '../posts/posts?query=services'
    })
  },
  /**
   * 点击单个商品的选择button
   */
  bindSelectTap: function(e) {
    var data = this.data.data
    var selected_data = e.detail.value
    //根据选择商品项目来重置数据源
    data.forEach(function(value, index, array) {
      if (selected_data.indexOf("" + index) < 0) {
        array[index].checked = false
      } else {
        array[index].checked = true
      }
    })
    //重置数据源
    this.setData({
      data: data
    })
    this.computeGoodsCount() //重新计算购物车商品数量
    this.computeGoodsTotalPrice() //重新计算购物车总价格
  },
  /**
   * 点击选择全部商品的button
   */
  bindSelectAllTap: function(e) {
    //处理全部选择商品
    if (e.detail.value == 'true') {
      this.data.data.forEach(function(value, index, array) {
        array[index].checked = true;
      })
    } else {
      this.data.data.forEach(function(value, index, array) {
        array[index].checked = false;
      })
    }
    //重置数据源
    this.setData({
      data: this.data.data
    })
    this.computeGoodsCount() //重新计算购物车商品数量
    this.computeGoodsTotalPrice() //重新计算购物车总价格
  },
  /**
   * 删除全部商品
   */
  bindDeleteGoodsTap: function(e) {
    var data = this.data.data
    var that = this
    //获取选择的商品的hash的数组
    var hash = that.getSelectedGoodsFormCart()
    if (hash.split(',').length > 0 && hash != '') {
      //弹出提示窗
      wx.showModal({
        content: _('是否删除选中商品？'),
        success: function(res) {
          if (res.confirm) {
            //如果选择同意，则去调用删除商品函数
            that.deleteGoodsFormCart({
              hash: hash,
              success: function(res) {
                //如果后台删除成功，则页面上删除商品元素
                if (res.data.success) {
                  var index = data.length;
                  while (index--) {
                    if (data[index].checked) {
                      data.splice(index, 1);
                    }
                  }
                  //重置数据源
                  that.setData({
                    data: data
                  })
                  that.goodsCount()
                  that.computeGoodsCount() //重新计算商品数量
                  that.computeGoodsTotalPrice() //重新计算购物车总价格
                }
                //弹出提示信息
                wx.showToast({
                  title: res.data.message,
                  icon: 'none',
                  duration: 2000
                })
              }
            })
          }
        }
      })
    } else {
      //弹出提示信息
      wx.showToast({
        title: _('还未选择商品！'),
        icon: 'none',
        duration: 2000
      })
    }
  },
  /**
   * 处理点击商品数量减少按钮
   */
  numberMinusTap: function(e) {
    var that = this
    var row = e.currentTarget.dataset.row;
    var hash = e.currentTarget.dataset.hash;
    //调用操作购物车函数
    this.operaCart({
      hash: hash,
      goodsnumber: -1,
      success: function(res) {
        //调用成功，显示信息窗
        if (res.data.success) {
          wx.showToast({
            title: res.data.message,
            icon: 'none',
            duration: 2000
          })
          that.data.data[row].goods_number = that.data.data[row].goods_number > 1 ? that.data.data[row].goods_number - 1 : 1;
          //重置数据
          that.setData({
            data: that.data.data
          })
          that.computeGoodsCount() //重新计算购物车商品数量
          that.computeGoodsTotalPrice() //重新计算购物车总价格
        }
      }
    })
  },
  /**
   * 处理点击商品数量增加按钮
   */
  numberPlusTap: function(e) {
    var that = this
    var row = e.currentTarget.dataset.row;
    var hash = e.currentTarget.dataset.hash;
    //调用操作购物车函数
    this.operaCart({
      hash: hash,
      goodsnumber: 1,
      success: function(res) {
        if (res.data.success) {
          wx.showToast({ //显示错误提示
            title: res.data.message,
            icon: 'none',
            duration: 2000
          })
          that.data.data[row].goods_number = that.data.data[row].goods_number + 1;
          //重置数据
          that.setData({
            data: that.data.data
          })
          that.computeGoodsCount() //重新计算购物车商品数量
          that.computeGoodsTotalPrice() //重新计算购物车总价格
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    //清空购物车提交的商品
    app.globalData.orderGoods = ''
    //显示加载窗
    wx.showLoading({
      title: _('加载中'),
      success: function() {
        //窗口滚动到最顶端
        wx.pageScrollTo({
          scrollTop: 0,
          duration: 0
        })
      }
    })
    var that = this;
    var openid = app.globalData.openid
    var user_id = app.globalData.user_id
    //重新获取购物车商品信息
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetShareCartByOpenid',
      form_data: {
        user_id: user_id,
        openid: openid
      },
      success: function(res) {
        if (res.data.data) {
          that.setData({
            data: res.data.data
          })
        } else {
          wx.showToast({ //显示错误提示
            title: '获取数据失败',
            icon: 'none',
            duration: 2000
          })
        }
        that.setData({
          show_flag: true,
          is_sharer: app.globalData.is_sharer
        })
        that.goodsCount()
        that.computeGoodsCount()
        that.computeGoodsTotalPrice()
      },
      complete: function() {
        wx.hideLoading()
      }
    })
  },
  /**
   * 重新商品总数用来判断购物车里是否为空
   */
  goodsCount: function() {
    var sum = 0
    console.log(this.data.data)
    if (this.data.data) {
      this.data.data.forEach(function(value, index, array) {
        sum += value.goods_number;
      })
    }
    this.setData({
      goods_total: sum
    })
  },
  /**
   * 重新计算和更新商品总数
   */
  computeGoodsCount: function() {
    var sum = 0
    var selected_count = 0
    if (this.data.data) {
      this.data.data.forEach(function(value, index, array) {
        if (value.checked) {
          sum += value.goods_number;
          selected_count++
        }
      })
    }
    this.setData({
      goodscount: sum,
      selected_count: selected_count
    })
  },
  /**
   * 重新计算和更新商品总价格
   */
  computeGoodsTotalPrice: function() {
    var sum = 0
    if (this.data.data) {
      this.data.data.forEach(function(value, index, array) {
        if (value.checked) {
          sum += value.goods_shop_price * value.goods_number
        }
      })
    }
    this.setData({
      goodsprice: sum.toFixed(2)
    })
  },
  /**
   * 操作购物车增减
   */
  operaCart: function(a) {
    var that = this
    var user_id = app.globalData.user_id
    var share_user_id = app.globalData.share_user_id
    var openid = app.globalData.openid
    utils.requestQueryByPost({
      url: '/mobile/wechat/operateCart',
      form_data: {
        user_id: user_id,
        share_user_id: share_user_id,
        hash: a.hash,
        openid: openid,
        total: a.goodsnumber
      },
      success: a.success
    })
  },
  /**
   * 从购物车中删除商品
   */
  deleteGoodsFormCart: function(a) {
    var that = this
    var user_id = app.globalData.user_id
    var openid = app.globalData.openid
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxDeleteCarts',
      // method: 'POST',
      // dataType: 'json',
      // traditional:  true,
      form_data: {
        user_id: user_id,
        hash: a.hash,
        openid: openid
      },
      success: a.success
    })
  },
  /**
   * 组合选择的购物车商品 
   */
  getSelectedGoodsFormCart: function() {
    var select_array = ''
    var data = this.data.data
    data.forEach(function(value, index, array) {
      if (data[index].checked) {
        select_array += data[index].hash + ','
      }
    })
    //删除组合后的商品hash列表最后的多余符号
    select_array = select_array.substring(0, select_array.lastIndexOf(','));
    return select_array
  },
  onReachBottom: function() {
    if (this.data.current_page < this.data.total_page) {
      var page = this.data.current_page + 1;
      this.setData({
        show_loading: true,
        page: page
      })
      this.getCommendGoodsList()
    }
  },
  getCommendGoodsList: function() {
    var that = this
    var openid = app.globalData.openid
    var goods_data = this.data.commendlist
    var share_user_id = app.globalData.share_user_id
    that.setData({
      is_sharer: app.globalData.is_sharer
    })

    //获取推荐商品列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetGoodsList',
      form_data: {
        user_id: app.globalData.user_id,
        openid: openid,
        share_user_id: share_user_id,
        is_sharer: that.data.is_sharer,
        page: that.data.page,
        is_commend: 1
      },
      success: function(response) {
        if (response.data.success) {

          for (var append_index in response.data.data.goods_list) {
            goods_data.push(response.data.data.goods_list[append_index]);
          }

          that.setData({
            commendlist: goods_data,
            show_loading: false,
            current_page: response.data.data.current_page,
            total_page: response.data.data.total_page,
            total_items: response.data.data.total
          })
        }
        wx.hideLoading()
      }
    })
  }
})