//dominodetail.js
//获取应用实例
const app = getApp()
const utils = require('../../utils/util.js')
const _ = utils._; //翻译函数

Page({
   data: {
      goods: {},
      show_flag: false,      
      page: 1,
      show_loading: true,
      current_page: 1,
      total_page: 1,
      show_flag: false,
      shop_data: {},
      show_authorize: false,
      total_price: 0.00,
      total_number: 0,
      user_list: {},
      is_share: false, 
      pictrue_index: 0,
      select_array: '',
      select_goods_number: ''
   },
   onLoad: function(options) {

     this.setData({
       _t: utils._t(), //翻译
     });

      var that = this
      var user_id = ''
      let share_user_id = ''
      let dominoid = ''
      var openid = app.globalData.openid

      //如果来源于二维码扫码url参数解析
      if (options.q !== undefined) {
         var scan_url = decodeURIComponent(options.q);
         var result = utils.parseURL(scan_url);
         user_id = result.s
         share_user_id = result.r
         dominoid = result.h
      } else { //否则来源于页面迁移和分享点击
         user_id = options.s
         share_user_id = options.r
         dominoid = options.d
      }
     if (!app.globalData.show_authorize) {
       wx.showLoading({
         title: _('加载中'),
       })
     }
      app.thisAuthorizeCallback = rp => {
         that.setData({
            show_authorize: app.globalData.show_authorize
         })
      }
      
      app.globalData.user_id = user_id

      if (share_user_id !== null && share_user_id !== undefined && share_user_id !== '') {
         app.globalData.share_user_id = share_user_id
      }

      //options.scene = true //测试扫小程序码使用 十里平湖霜满天 寸寸青丝愁华年 对月形单望相互 只羡鸳鸯不羡仙
      console.log(options)
      if (options.scene) {
         var domino_share_hash = decodeURIComponent(options.scene)
         console.log('domino_share_hash:' + domino_share_hash)
         utils.requestQueryByPost({
            url: '/mobile/wechat/ajaxGetUserIdByDominoHash',
            form_data: {
               domino_share_hash: domino_share_hash
            },
            success: function (res) {
               if (res.data.success) {
                  user_id = res.data.data.user_id
                  if (user_id !== null && user_id !== undefined && user_id !== '') {
                     app.globalData.user_id = user_id
                  } else {
                     user_id = app.globalData.user_id
                  }
                  share_user_id = res.data.data.share_user_id
                  if (share_user_id !== null && share_user_id !== undefined && share_user_id !== '') {
                     app.globalData.share_user_id = share_user_id
                  } else {
                     share_user_id = app.globalData.share_user_id
                  }
                  that.initData(user_id, res.data.data.domino_id)
               } else {
                  user_id = app.globalData.user_id
                  that.initData(user_id, res.data.data.domino_id)
               }
            }
         })
      } else {
         that.initData(user_id, dominoid)
      }
   },
   initData: function (user_id, dominoid) {
      var that = this
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetShopInfo',
         form_data: {
            user_id: user_id
         },
         success: function (res) {
            if (res.data.success) {
               that.setData({
                  shop_data: res.data.data
               })
            }
         }
      })
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetDominoDetail',
         form_data: {
            domino_id: dominoid,
            user_id: user_id,
            share_user_id: app.globalData.share_user_id
         },
         success: function (res) {
            if (res.data.success) {
               that.setData({
                  goods: res.data.data,
                  show_flag: true
               })
               user_id = res.data.data.user_id
            } else {
               user_id = app.globalData.user_id
            }
            app.globalData.domino_id = that.data.goods.domino_id
         },
         complete: function () {
            wx.hideLoading()
            that.setData({
               show_flag: true
            })
         }
      })
      utils.requestQueryByPost({
         url: '/mobile/wechat/ajaxGetDominoUserList',
         form_data: {
            domino_id: dominoid
         },
         success: function (res) {
            if (res.data.success) {
               that.setData({
                  user_list: res.data.data
               })
            }
         }
      })
   }, 
   bindPreviewImageTap: function (e) {
      var that = this
      var image = e.currentTarget.dataset.forindex;
      wx.previewImage({
         current: image, // 当前显示图片的http链接
         urls: that.data.goods.domino_pictures // 需要预览的图片http链接列表
      })
   },
   numberMinusTap: function (e) {
      var i = e.currentTarget.dataset.forindex;
      var data = this.data.goods
      data.domino_goods[i].goods_number = data.domino_goods[i].goods_number > 0 ? data.domino_goods[i].goods_number - 1 : 0
      this.setData({
         goods: data
      })
      this.computePriceAndNumber() //重新计算总价和数量
   },
   numberPlusTap: function (e) {
      var i = e.currentTarget.dataset.forindex;
      var data = this.data.goods
      data.domino_goods[i].goods_number = data.domino_goods[i].goods_number + 1
      this.setData({
         goods: data
      })
      this.computePriceAndNumber() //重新计算总价和数量
   },
   computePriceAndNumber: function() {
      var data = this.data.goods.domino_goods
      var total_price = 0
      var total_number = 0
      //根据选择商品项目来重置数据源
      data.forEach(function (value, index, array) {
         if( value.goods_number > 0 ) {
            total_number += value.goods_number
            total_price += value.goods_price * value.goods_number
         }
      })
      this.setData({
         total_price: total_price.toFixed(2),
         total_number: total_number
      })
   },
   bindGoodsDetailViewTap: function(e) {
      var hash = e.currentTarget.dataset.hash; //获取按钮设置的数据
      var share_user_id = app.globalData.share_user_id
      wx.navigateTo({
         url: '../detail/detail?r=' + share_user_id +'&h=' + hash + '&d=domino'
      })
   },
   bindToHomeTap: function() {
      wx.switchTab({
         url: '../index/index'
      })
   },
   bindGoDominoListTap: function() {
      var user_id = app.globalData.user_id
      wx.navigateTo({
         url: '../domino/domino?s=' + user_id
      })
   },
   bindBackViewTap: function() {
      wx.navigateBack({})
   },
   bindDirectPayViewTap: function() {
      // //获取选择的商品的hash的数组
      var openid = app.globalData.openid
      var that = this

      if (that.data.total_number > 0 ) {
         //获取选择的商品的hash的数组
         that.getSelectedGoodsFormThisPage()

         console.log(that.data.select_array)
         console.log(that.data.select_goods_number)

         app.globalData.orderGoods = that.data.select_array
         app.globalData.orderGoodsNumber = that.data.select_goods_number

         utils.requestQueryByPost({
            url: '/mobile/wechat/ajaxGetDefalutAddress',
            form_data: {
               openid: openid
            },
            success: function (res) {
               app.globalData.defalutAddress = res.data.data
               wx.navigateTo({
                  url: '../confirm/confirm' + '?d=domino'
               })
            }
         })
      } else {
         wx.showToast({ //显示返回结果
            title: _('您还为选择接龙商品！'),
            icon: 'none',
            duration: 3000
         })
      }
   },
   /**
    * 组合选择的商品 
    */
   getSelectedGoodsFormThisPage: function () {
      var select_array = ''
      var select_goods_number = ''
      var data = this.data.goods.domino_goods
      data.forEach(function (value, index, array) {
         if (value.goods_number > 0) {
            select_array += data[index].goods_hash + ','
            select_goods_number += value.goods_number + ','
         }
      })
      //删除组合后的商品hash列表最后的多余符号
      select_array = select_array.substring(0, select_array.lastIndexOf(','));
      select_goods_number = select_goods_number.substring(0, select_goods_number.lastIndexOf(','));

      this.setData({
         select_array: select_array,
         select_goods_number: select_goods_number
      })
   },
   onShareAppMessage: function (res) {
      var dominoid = this.data.goods.domino_id;
      var title = this.data.goods.domino_title;
      var user_id = app.globalData.user_id
      var share_user_id = app.globalData.share_user_id
      if (res.from === 'button') {
         // 来自页面内转发按钮
      }
      return {
         title: title,
         path: '/pages/dominodetail/dominodetail?r=' + share_user_id + '&s=' + user_id + '&d=' + this.data.goods.domino_id,
         imageUrl: this.data.goods.share_image
      }
   },
   bindGoShareFriendsTap: function(e) {
      var that = this
      that.setData({
         is_share: true
      })
   },
   onCloseShareCode: function (e) {
      var that = this
      that.setData({
         is_share: false
      })
   }, 
   onSaveShareCode: function (e) {
      var that = this
      wx.showLoading({
         title: _('保存图片中'),
      })
      that.operaDownloadPicture()
   },
   /**
    * 下载商品图片并调用处理函数去生成分享用图片
    */
   operaDownloadPicture: function () {
      var that = this
      var picture = that.data.goods.domino_pictures[that.data.pictrue_index]
      if (typeof (picture) !== "undefined" && that.data.pictrue_index < that.data.goods.domino_pictures.length) {
         wx.downloadFile({
            url: picture,
            success: function (res) {
               var imageFile = res.tempFilePath
               wx.saveImageToPhotosAlbum({
                  filePath: imageFile,
                  success: function(e) {
                     that.setData({
                        pictrue_index: that.data.pictrue_index + 1
                     })
                     if (that.data.pictrue_index < that.data.goods.domino_pictures.length) {
                        that.operaDownloadPicture() //这里使用了递归，修改代码要注意保护
                     }
                  }
               })
            }
         })
      }
      if (that.data.pictrue_index == that.data.goods.domino_pictures.length - 1) {
         picture = that.data.goods.share_friends_image
         wx.downloadFile({
            url: picture,
            success: function (res) {
               var imageFile = res.tempFilePath
               wx.saveImageToPhotosAlbum({
                  filePath: imageFile,
                  success: function (e) {
                     wx.hideLoading()
                     wx.showToast({
                        title: _('图片保存成功，请前往相册中查看！'),
                        icon: 'none',
                        duration: 3000
                     })
                  }
               })
            }
         })
      }
   },
   bindGetUserInfo: function (e) {
      if (e.detail.userInfo) {
         //用户按了允许授权按钮 
         var that = this;
         //插入登录的用户的相关信息到数据库 
         utils.requestQueryByPost({
            url: '/mobile/wechat/ajaxCheckWeixinUserInfo',
            form_data: {
               openid: app.globalData.openid,
               avatarUrl: e.detail.userInfo.avatarUrl,
               city: e.detail.userInfo.city,
               country: e.detail.userInfo.country,
               gender: e.detail.userInfo.gender,
               language: e.detail.userInfo.language,
               nickName: e.detail.userInfo.nickName,
               province: e.detail.userInfo.province,
               unionid: app.globalData.unionid
            },
            success: function (res) {
               //从数据库获取用户信息 
               //that.queryUsreInfo();
               console.log("插入小程序登录用户信息成功！");
            }
         });

         //授权成功后关闭遮盖 
         app.globalData.show_authorize = false
         that.setData({
            show_authorize: false
         })
      } else {
         //用户按了拒绝按钮 
         wx.showModal({
            title: _('警告'),
            content: _('您点击了拒绝授权，将无法进入小程序，请授权之后再进入!!!'),
            showCancel: false,
            confirmText: _('返回授权'),
            success: function (res) {
               if (res.confirm) {
                  console.log('用户点击了“返回授权”')
               }
            }
         })
      }
   }
})