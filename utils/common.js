const app = getApp()
const utils = require('util.js')

//处理加入购物车
let addGoodsToCart = function (user_id, share_user_id, openid, goods_hash, goods_quantity ) {
   wx.showLoading({
      title: '加入购物车中',
   })
   utils.requestQueryByPost({
      url: '/mobile/wechat/operateCart',
      form_data: {
         user_id: user_id,
         hash: goods_hash,
         share_user_id: share_user_id,
         openid: openid,
         total: goods_quantity
      },
      success: function (res) {
         wx.showToast({
            title: res.data.message,
            icon: 'none',
            duration: 2000
         })
      },
      fail: function () {
         wx.hideLoading()
      }
   })
}

module.exports = {
   addGoodsToCart: addGoodsToCart
}