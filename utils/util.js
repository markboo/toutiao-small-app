
//定义日期工具
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

//格式化数字类型
const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

//格式化数字类型，加入千分位，小数点后保留两位
const toFormatNumber = fn => {
  var n = fn.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'); //使用正则替换，每隔三个数加一个','
  return n
}

//获取url请求中的参数和值
let getQueryString = function(url, name) {
  var reg = new RegExp('(^|&|/?)' + name + '=([^&|/?]*)(&|/?|$)', 'i')
  var r = url.substr(1).match(reg)
  if (r != null) {
    console.log("r = " + r)
    console.log("r[2] = " + r[2])
    return r[2]
  }
  return null;
}

const parseURL = (url) => {
  if (url && url.indexOf("?") == -1) return {}
  var startIndex = url.indexOf("?") + 1;
  var str = url.substr(startIndex);
  var strs = str.split("&");
  var param = {}
  for (var i = 0; i < strs.length; i++) {
    var result = strs[i].split("=");
    var key = result[0];
    var value = result[1];
    param[key] = value;
  }
  return param
}

//发起网络Post类型请求
let requestQueryByPost = function(request) {
  request.form_data.language = wx.getStorageSync('Language') || 'zh_CN'
  request.form_data.source = 'toutiao'
  let task = tt.request({
        url: 'https://www.eqiyu.com' + request.url,
        method: 'POST',
        dataType: 'json',
        data: request.form_data,
        header: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        success: request.success,
        fail: request.fail,
        complete: request.complete
    });

}

function getLanguage() {
  //返回缓存中的language属性 (en / zh_CN) 	
  return tt.getStorageSync('Language') || 'zh_CN'
};

function translate() {
  //返回翻译的对照信息
  return require('..//i18n/' + getLanguage() + '.js').languageMap;
}

function translateTxt(desc) {
  //翻译	
  return translate()[desc] || desc;
}

module.exports = {
  getQueryString: getQueryString,
  requestQueryByPost: requestQueryByPost,
  formatTime: formatTime,
  parseURL: parseURL,
  getLanguage: getLanguage,
  _t: translate,
  _: translateTxt,
}