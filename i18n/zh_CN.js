const languageMap = {
  '【消费提示】为取得良好的购物体验，下单前，请认真阅读文字信息！': '【消费提示】为取得良好的购物体验，下单前，请认真阅读文字信息！',
  '1、拍错、多拍、不想要了；': '1、错拍、多拍、不想要了；',
  '2、协商一致取消订单；': '2、协商一致取消订单；',
  '2人开团': '2人开团',
  '3、因为缺货；': '3、因为缺货；',
  '4、未按照约定时间发货；': '4、未按照约定时间发货；',
  '5、操作失误造成误下单（商品数量、送货地址等选错）;': '5、操作失误造成误下单（商品数量、送货地址等选错）；',
  '6、操作失误造成误下单；': '6、操作失误造成下单；',
  '包邮': '包邮',
  '保存': '保存',
  '保存商品主图与此图片': '保存商品主图与此图片',
  '保存图片中': '保存图片中',
  '本次提现金额': '本次提现金额',
  '本月成交': '本月成交',
  '编辑地址': '编辑地址',
  '标签': '标签',
  '参团购买': '参团购买',
  '参与了接龙': '参与了接龙',
  '查看订单': '查看订单',
  '产品说明': '产品说明',
  '承运商': '承运商',
  '处理中': '处理中',
  '次 收藏': '次 收藏',
  '凑单': '凑单',
  '促销活动商品': '促销活动商品',
  '待付款': '待付款',
  '待支付金额': '待支付金额',
  '当前计重': '当前计重',
  '地址管理': '地址管理',
  '点击查看明细': '点击查看明细',
  '点击去提现': '点击去提现',
  '点击显示商品详情': '点击显示商品详情',
  '电话': '电话',
  '订单编号': '订单编号',
  '订单号': '订单号',
  '订单金额': '订单金额',
  '订单取消提示': '订单取消提示',
  '订单日期': '订单日期',
  '订单填写': '订单填写',
  '订单详情': '订单详情',
  '订单运费：': '订单运费：',
  '订单支付': '订单支付',
  '订单状态': '订单状态',
  '订单总价：': '订单总价：',
  '发布': '发布',
  '发货仓：': '发货仓：',
  '发货地': '发货地',
  '返回到我分享的店铺': '返回到我分享的店铺',
  '返回授权': '返回授权',
  '返回页顶': '返回页顶',
  '访问店铺首页': '访问店铺首页',
  '放入购物车': '放入购物车',
  '粉丝 3456人 接龙 129人': '粉丝 3456人 接龙 129人',
  '服务电话': '服务电话',
  '服务条款': '服务条款',
  '复制': '复制',
  '该商品已经下架': '该商品已经下架',
  '该商品已收藏过！': '该商品已收藏过！',
  '更多商品': '更多商品',
  '购买商品参与': '购买商品参与',
  '购物车': '购物车',
  '关于此应用': '关于此应用',
  '国内发货': '国内发货',
  '还未选择商品！': '还未选择商品！',
  '海外直邮': '海外直邮',
  '海外直邮，正品进口，商品列表分享！': '海外直邮，正品进口，商品列表分享！',
  '合计': '合计',
  '获取您的公开信息（昵称、头像等）': '获取您的公开信息（昵称、头像等）',
  '继续支付': '继续支付',
  '加载中': '加载中',
  '价格': '价格',
  '价格更新': '价格更新',
  '件': '件',
  '警告': '警告',
  '巨划算': '巨划算',
  '开户行（省）': '开户行（省）',
  '开户行（市）': '开户行（市）',
  '开户行名': '开户行名',
  '开户行支行': '开户行支行',
  '可提现金额': '可提现金额',
  '快递单号': '快递单号',
  '历史总收入': '历史总收入',
  '联系方式': '联系方式',
  '联系方式手机号码输入不合法，请重新输入': '联系方式手机号码输入不合法，请重新输入',
  '留言': '留言',
  '默认': '默认',
  '您的购物车是空的呢': '您的购物车是空的呢',
  '您点击了拒绝授权，将无法进入小程序，请授权之后再进入': '',
  '您还没有收藏的商品呢': '',
  '您还没有选择商品！': '',
  '您还为选择接龙商品！': '',
  '品牌分类': '品牌分类',
  '品牌选择': '品牌选择',
  '请升级微信版本': '请升级微信版本',
  '请输入100的整数倍': '请输入100的整数倍',
  '请输入银行名称': '请输入银行名称',
  '请输入银行账号': '请输入银行账号',
  '请输入支行（分行）名称': '请输入支行（分行）名称',
  '请选择城市': '请选择城市',
  '请选择省份': '请选择省份',
  '请选择收货人地址': '请选择收货人地址',
  '请选择以下取消订单的原因：': '请选择以下取消订单的原因：',
  '请选择原因！': '请选择原因！',
  '取消订单': '取消订单',
  '去结算': '去结算',
  '去支付': '去支付',
  '全部订单': '全部订单',
  '确认': '确认',
  '筛选': '筛选',
  '删除': '删除',
  '商品分类': '商品分类',
  '商品价格：': '商品价格',
  '商品接龙': '商品接龙',
  '商品类型：': '商品类型：',
  '商品列表': '商品列表',
  '商品数量：': '商品数量：',
  '商品总价：': '商品总价：',
  '上月': '上月',
  '上月成交': '上月成交',
  '上月收益': '上月收益',
  '本月收益': '本月收益',
  '设置提现收款账户': '设置提现收款账户',
  '申请获取以下权限': '申请获取以下权限',
  '身份证反面': '身份证反面',
  '身份证号码': '身份证号码',
  '身份证正面': '身份证正面',
  '神仙水': '神仙水',
  '是否删除选中商品？': '是否删除选中商品？',
  '收货地址': '收货地址',
  '收货地址管理': '收货地址管理',
  '收货人': '收货人',
  '收货人电话': '收货人电话',
  '收货人身份证：': '收货人身份证：',
  '收货人未选择': '收货人未选择',
  '收件人身份证': '收件人身份证',
  '首重/运费': '首重/运费',
  '售后服务': '售后服务',
  '授权登录': '授权登录',
  '所在地区': '所在地区',
  '提交': '提交',
  '提交的地址信息数据不能为空！': '提交的地址信息数据不能为空！',
  '提交中': '提交中',
  '提现': '提现',
  '提现处理中金额': '提现处理中金额',
  '提现明细': '提现明细',
  '提现设置': '提现设置',
  '提现中金额': '提现中金额',
  '填写身份证号的同时需要上传身份证照片！': '填写身份证号的同时需要上传身份证照片！',
  '图片保存成功，请前往相册中查看！': '图片保存成功，请前往相册中查看！',
  '微信在线支付': '微信在线支付',
  '微信支付': '微信支付',
  '未发现接龙商品': '未发现接龙商品',
  '未搜索到结果': '未搜索到结果',
  '我的': '我的',
  '我的订单': '我的订单',
  '我的收藏': '我的收藏',
  '我的收藏商品': '我的收藏商品',
  '我的账单': '我的账单',
  '我的账户': '我的账户',
  '我去开团': '我去开团',
  '无补贴优惠': '无补贴优惠',
  '物流跟踪': '物流跟踪',
  '下单时间': '下单时间',
  '下月': '下月',
  '详细地址': '详细地址',
  '消费安全提示': '消费安全提示',
  '新建地址': '新建地址',
  '新品上架': '新品上架',
  '信息安全': '信息安全',
  '信息安全说明': '信息安全说明',
  '修改': '修改',
  '续重/运费': '续重/运费',
  '选择的商品中有下架商品，不能提交购物车！': '选择的商品中有下架商品，不能提交购物车！',
  '选中商品': '选中商品',
  '已发货': '已发货',
  '以下为购买进口商品时必填项': '以下为购买保税和海外直邮商品时必填项',
  '银行账号': '银行账号',
  '银行开户名':'银行开户名',
  '请输入开户人姓名': '请输入开户人姓名',
  '请输入银行预留手机号': '请输入银行预留手机号',
  '隐私条款': '隐私条款',
  '佣金': '佣金',
  '用户取消支付，请重新支付！': '用户取消支付，请重新支付！',
  '有102人看过 24人参与': '有102人看过 24人参与',
  '预留手机号': '预留手机号',
  '元': '元',
  '原产国': '原产国',
  '月度收入明细': '月度收入明细',
  '月度提现明细': '月度提现明细',
  '运费标准：': '运费标准：',
  '运费标准': '运费标准',
  '运费补贴优惠': '运费补贴优惠',
  '运送天数': '运送天数',
  '再购买': '再购买',
  '账户总余额': '账户总余额',
  '折扣金额：': '折扣金额：',
  '支付成功': '支付成功',
  '支付处理中': '支付处理中',
  '支付方式': '支付方式',
  '支付失败，请继续支付！': '支付失败，请继续支付！',
  '支付状态': '支付状态',
  '支付总金额': '支付总金额',
  '直接购买': '直接购买',
  '种': '种',
  '重置': '重置',
  '转发': '转发',
  '子订单商品总价格': '子订单商品总价格',
  '子订单状态': '子订单状态',
  '总合计（不含运费）': '总合计（不含运费）',
  '最大金额': '最大金额',
  '最小金额': '最小金额',
  '昨天': '昨天',
  'SKII神仙水': 'SKII神仙水',
  '切换语言（日本语）':'切换语言（日本语）',
  '首页': '首页',
  '分类': '分类',
  '选择类型': '选择类型',
  '清除缓存': '清除缓存',
  '使用说明': '使用说明',
  '更多内容': '更多内容'
}

module.exports = {
  languageMap: languageMap
}