//app.js
const utils = require('./utils/util.js')

App({
  onLaunch: function (options) {
    var that = this

    //登录
    tt.login({
      success: res => {
        //发送res.code 到后台换取 openId, sessionKey, unionId
        utils.requestQueryByPost({
          url: '/mobile/wechat/ajaxGetToutiaoOpenid',
          form_data: {
            code: res.code,
            source: 'toutiao'
          },
          success: function (response) {
            if (response.data.data.openid) {
              that.globalData.openid = response.data.data.openid
            }
            if (response.data.data.session_key) {
              that.globalData.session_key = response.data.data.session_key
            }
            if (response.data.data.is_sharer) {
              that.globalData.is_sharer = response.data.data.is_sharer
            }

            console.log('ajaxGetToutiaoOpenid')
            console.log(response.data.data.openid)
            console.log('ajaxGetToutiaoOpenid')

            tt.getSetting({ //获取用户信息
              success: rp => {
                if (rp.authSetting['scope.userInfo']) {
                  tt.getUserInfo({
                    success(res) {
                      console.log(`getUserInfo调用成功`);
                      console.log(res.userInfo.avatarUrl);
                      that.globalData.userInfo = res.userInfo
                      //如果缓存中有语言设定则使用缓存中设定，如果没有则根据用户微信的语言
                      var local_language = tt.getStorageSync('Language')
                      if (!local_language) {
                        if (local_language) {
                          var language = local_language == 'zh_CN' ? 'zh_CN' : 'jp'
                          tt.setStorageSync('Language', language)
                          tt.reLaunch({
                            url: '../index/index',
                          });
                        }
                      }
                    },
                    fail(res) {
                      console.log(`getUserInfo调用失败`);
                    }
                  })
                  that.globalData.show_authorize = false
                } else {
                  that.globalData.show_authorize = true
                }
                if (that.thisAuthorizeCallback) {
                  that.thisAuthorizeCallback(rp)
                }
                console.log('1:' + that.globalData.show_authorize)
              }
            })
            //由于 ajaxGetWeixinOpenid 会在 Page.onLoad 之后才返回
            //所以此处加入 callback 以防止这种情况
            if (that.thisOpenidCallback) {
              that.thisOpenidCallback(response)
            }
          }
        })
      }
    })


    //获取订单状态列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetOrderStatusFilter',
      form_data: {},
      success: function (res) {
        if (res.data.success) {
          that.globalData.orderStatus = res.data.data
        }
      }
    })

    //获取订单取消原因列表
    utils.requestQueryByPost({
      url: '/mobile/wechat/ajaxGetOrderCancelReason',
      form_data: {},
      success: function (res) {
        if (res.data.success) {
          that.globalData.cancelReason = res.data.data
        }
      }
    })

  },
  globalData: {
    userInfo: null, //保存用户信息
    userId: null, //保存用户id
    addressId: null, //保存地址选择页面返回时地址hash
    orderGoods: '', //保存订单选择商品列表
    openid: '', //'ojfY3twQlSjgkqabr-rd1aWuOHU8', //测试用openid
    unionid: '',
    share_user_id: '',
    user_id: '', //'1029747515215351808', //测试用userid
    defalutAddress: '', //默认地址，
    reload_flag: false,
    order_id: '',
    show_authorize: true,
    domino_id: '',
    shop_info: {},
    is_sharer: false, //是否是分享用户
    index_is_reload: false, //由于需要切换店铺判断是否需要刷新首页数据
    paymentData: {
      pay_status: '',
      pay_amount: 0.00,
      order_code: '',
      message: ''
    },
    orderStatus: {},
    cancelReason: {},
    server_url: "https://www.eqiyu.com"
  }
})